import mplayer
import mpaths
import mverbose
import mpyramid
import mAI
import mBcards
import mturn_history
import mcurses, curses
from random import shuffle

class Board:
    game_board=None
    def __init__(self, n):
        if n<2 or n>6:
            mverbose.verbose_f(mverbose.Verbose(),"error", "There should be [2-5] players")
            exit(1)
        self.players=n
        # mverbose.verbose_f(mverbose.Verbose(),"info", "There are",self.players,"players")
        if n==2:
            mverbose.verbose_f(mverbose.Verbose(),"error", "Unsupported board")
            exit(1)
        elif n==3:
            self.map_max_position=20
            self.img='img/map/map3_positions.jpg'
        elif n==4:
            mverbose.verbose_f(mverbose.Verbose(),"error", "Unsupported board")
            exit(1)
        elif n==5:
            self.map_max_position=32
            self.img='img/map/map5_positions.jpg'
        self.troops=[]
        self.paths=mpaths.Paths()
        self.temple=[3,10,15,21,28]
        self.obelisk=[1,3,8,10,15,16,21,23,28]
        self.pyramid=[4,5,6, 12,13,14, 18,19,20, 25,26,27, 30,31,32]
        self.wall=[[7,4,5,6], [11,12,13,14], [17,18,19,20], [24,25,26,27], [29,30,31,32]]
        #first is the position and the rest are the positions adjacents to it
        self.adjacent=[
                [1,2],
                [2,1,3],
                [3,2],
                [4,5,6,7],
                [5,4,6,7],
                [6,5,4,7],
                [7,4,5,6,8,9,15],
                [8,7,9,15,11],
                [9,7,8,10,11,16,17,21,24,29],
                [10,9],
                [11,8,9,12,13,14,15,16],
                [12,11,13,14],
                [13,11,12,14],
                [14,11,12,13],
                [15,7,8,11,16,17],
                [16,9,11,15,17],
                [17,9,15,16,18,19,20],
                [18,17,19,20],
                [19,17,18,20],
                [20,17,18,19],
                [21,9,22],
                [22,21,23,24,29],
                [23,22,24,28,29],
                [24,9,22,23,25,26,27,28],
                [25,24,26,27],
                [26,24,25,27],
                [27,24,25,26],
                [28,23,24,29],
                [29,9,22,23,28,30,31,32],
                [30,29,31,32],
                [31,29,30,32],
                [32,29,30,31]
        ]
        if Board.game_board==None:
            Board.game_board=self


    def sort_troops_str_f(self,troops, reverse=False):
        for z in troops:
            for x in troops:
                if z.figure_strength+z.quantity>x.quantity+x.figure_strength:
                    index_z=troops.index(z)
                    index_x=troops.index(x)
                    if (reverse and index_z<index_x) or (not reverse and index_z>index_x):
                        temp=x
                        troops[index_x]=z
                        troops[index_z]=temp
        return troops

    def aprox_troop_strength_f(self,defender_troop,attacker_troop):
        troop_margin=defender_troop.quantity
        if defender_troop.player.has_tile_f('Defensive_victory'):
            troop_margin+=2
        if defender_troop.player.has_tile_f('Offensive_Strategy') or defender_troop.player.has_tile_f('Defensive_strategy'):
            troop_margin+=1
        if defender_troop.player.has_tile_f('Blades_of_Neith'):
            troop_margin+=1
        if defender_troop.player.has_tile_f('Defense'):
            troop_margin+=1
        if attacker_troop.player.AI.choose_f(0,100)>100-len(defender_troop.player.DIcards)*20:
            troop_margin+=1

        if attacker_troop.player.AI.choose_f(0,100)>100-len(attacker_troop.player.DIcards)*20:
            troop_margin-=1
        if attacker_troop.player.has_tile_f('Blades_of_Neith'):
            troop_margin-=1
        if attacker_troop.player.has_tile_f('Charge'):
            troop_margin-=1
        if attacker_troop.player.has_tile_f('Offensive_Strategy') or attacker_troop.player.has_tile_f('Defensive_strategy'):
            troop_margin-=1
        if attacker_troop.player.has_tile_f('Initiative'):
            troop_margin-=2
        if not attacker_troop.figure_nullify:
            troop_margin+=defender_troop.figure_strength
        return troop_margin

    def get_vulnerable_troop_AI_f(self,player,AI_friendly=True):
        interesting=[]
        troops=troops=self.get_troops_f(player=player)
        if len(troops)==0:
            return None,None
        troops=self.sort_troops_str_f(troops)
        best=troops[0]
        random_margin=player.AI.choose_f(1,2)
        for i in self.troops:
            if (AI_friendly and i.player.AI!=None) or player==i.player:
                continue
            if random_margin+self.aprox_troop_strength_f(i,best)<best.quantity+best.figure_strength:
                interesting.append(i)
        if len(interesting)==0:
            return None,None
        interesting=self.sort_troops_str_f(interesting,reverse=True)
        for i in troops:
            if i.position in player.city or i.position not in self.temple or player.AI.choose_f(0,100)>70:
                for j in interesting:
                    if j.position>self.map_max_position or i.position>self.map_max_position:
                        continue
                    if i.figure_strength+i.quantity>random_margin+self.aprox_troop_strength_f(j,i) and self.can_reach_f(j.position,i):
                        return i.position,j.position
        #check for empty city of enemies
        wall_copy=list(self.wall)
        shuffle(wall_copy)
        for i in wall_copy:
            if i[0]>self.map_max_position:
                continue
            pyramid1=mpyramid.Pyramids.game_pyramids.get_pyramid_f(i[1])
            if pyramid1.creator!=player.name and ("AI_" not in pyramid1.creator or ("AI_" in pyramid1.creator and not AI_friendly)):
                empty_city=True
                for j in i:
                    if not self.owned_by_someone_else_f(j,player,AI_friendly=AI_friendly):
                        empty_city=False
                        break
                if empty_city or player.has_tile_f('Defensive_victory') or mplayer.Player.P_counter*2<=mplayer.Player.AI_counter or player.AI.choose_f(0,100)>90:
                    for j in troops:
                        if (j.position==i[1] or j.position==i[2] or j.position==i[3]) and j.player.AI.choose_f(0,100)>10:
                            continue
                        if self.can_reach_f(pyramid1.position, j):
                            pyramid2=mpyramid.Pyramids.game_pyramids.get_pyramid_f(i[2])
                            pyramid3=mpyramid.Pyramids.game_pyramids.get_pyramid_f(i[3])
                            max_pyramid=None
                            if self.is_free_f(i[1]) and (max_pyramid==None or pyramid1.level>max_pyramid.level):
                                max_pyramid=pyramid1
                            if self.is_free_f(i[2]) and (max_pyramid==None or pyramid2.level>max_pyramid.level):
                                max_pyramid=pyramid2
                            if self.is_free_f(i[3]) and (max_pyramid==None or pyramid3.level>max_pyramid.level):
                                max_pyramid=pyramid3
                            if max_pyramid!=None:
                                return j.position,max_pyramid.position
        return None,None

    def get_interesting_positions_AI_f(self,player,taboo_part=None,AI_friendly=True):
        #TODO chance to invade pyramid by level
        interesting=[]
        troops=[]
        if taboo_part==None:
            taboo=[]
        else:
            taboo=list(taboo_part)
        for i in self.troops:
            taboo.append(i.position)
            if i.player==player or (AI_friendly and i.player.AI!=None):
                continue
            interesting.append(i.position)
        temples=list(self.temple)
        temples.append(1)
        for i in temples:
            if i in taboo:
                continue
            taboo.append(i)
            interesting.append(i)
        # if player.has_tile_f('Teleport'):
            # for i in self.obelisk:
                # if i in taboo:
                    # continue
                # taboo.append(i)
                # interesting.append(i)
        #checks if himself has vulnerable troops and the chance to move to unify them
        for i in troops:
            for j in troops:
                if i!=j and i.quantity+j.quantity<=player.troop_max:
                    if self.can_reach_f(i.position,j):
                        return j.position,i.position,taboo
                    if self.can_reach_f(j.position,i):
                        return i.position,j.position,taboo
        troops=troops=self.get_troops_f(player=player)
        if len(troops)==0 or len(interesting)==0:
            return None,None,taboo
        shuffle(interesting)
        troops=self.sort_troops_str_f(troops)
        for i in troops:
            if i.position in player.city or i.position not in self.temple or player.AI.choose_f(0,100)>70:
                for j in interesting:
                    if j>self.map_max_position or i.position>self.map_max_position:
                        continue
                    if self.can_reach_f(j,i):
                        return i.position,j,taboo
        #check for empty wall access (position 0), wall access to city checked in vulnerable function
        for i in self.wall:
            if i[0]>self.map_max_position:
                continue
            pyramid=mpyramid.Pyramids.game_pyramids.get_pyramid_f(i[1])
            if pyramid.creator!=player.name and ("AI_" not in pyramid.creator or ("AI_" in pyramid.creator and not AI_friendly)):
                for j in troops:
                    if self.can_reach_f(i[0],j) and self.is_free_f(i[0]):
                        return j.position,i[0],taboo
            taboo.append(i[0])
        return None,None,taboo

    def owned_by_someone_else_f(self,position,player,AI_friendly=False):
        for i in self.troops:
            if i.position==position:
                if player==i.player or ("AI_" in i.player.name and AI_friendly):
                    return False
                else:
                    return True
        return False

    def is_free_f(self,position):
        for i in self.troops:
            if i.position==position:
                return False
        return True

    def add_troop_f(self,troop):
        for i in self.troops:
            if i.position==troop.position:
                return 1
        self.troops.append(troop)

    def get_troop_f(self,position):
        for i in self.troops:
            if i.position==position:
                return i

    def count_figures_f(self, player, troops=None):
        if troops==None:
            troops=self.get_troops_f(player=player, figure_existing=True)
        return len(troops)+len(player.figures_free)

    def get_troops_f(self, player=None, possible_positions=None, figure_existing=None, AI_friendly=False, not_owner=None):
        if player==None:
            owner=None
        else:
            owner=player.name
        if player==None and possible_positions==None and figure_existing==None and AI_friendly==False and not_owner==None:
            return self.troops
        matches=[]
        for i in self.troops:
            if owner!=None and not_owner==None:
                if i.player.name!=owner:
                    continue
            if not_owner!=None:
                if i.player.name==not_owner:
                    continue
            if possible_positions!=None:
                if i.position not in possible_positions:
                    continue
            if figure_existing!=None:
                if figure_existing==False and i.figure!="":
                    continue
                if figure_existing==True and i.figure=="":
                    continue
            if AI_friendly and i.player.AI!=None:
                continue
            matches.append(i)
        return matches

    def list_troops_f(self,player=None):
        troops=self.get_troops_f(player)
        if player!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing troops for player",player.name)
        else:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing all troops")
        for i in troops:
            if i.figure:
                mverbose.verbose_f(mverbose.Verbose(),"info",
                    "\tplayer=", i.player.name,
                    "\tquantity=", i.quantity,
                    "\tposition=", i.position,
                    "\tdistance=", i.distance,
                    "\tfigure=", i.figure,
                    "\tfigure_strength=", i.figure_strength,
                    "\tfigure_defense=", i.figure_defense,
                    "\tfigure_damage=", i.figure_damage
                )
            else:
                mverbose.verbose_f(mverbose.Verbose(),"info",
                    "\tplayer=", i.player.name,
                    "\tquantity=", i.quantity,
                    "\tposition=", i.position
                )

    def get_troops_string_f(self, player=None, troops=None):
        if troops==None:
            troops=self.get_troops_f(player)
        if player!=None:
            string="Listing troops of player "+player.name+":\n"
        else:
            string="Listing all troops:\n"
        for i in troops:
            string=string+ "\tplayer="+ i.player.name+ "\tquantity="+ str(i.quantity)+ "\tposition="+ str(i.position)
            if i.figure:
                string=string+ "\tdistance="+ str(i.distance)+ "\tfigure="+ i.figure+ "\tfigure_strength="+ str(i.figure_strength)+ "\tfigure_defense="+ str(i.figure_defense)+ "\tfigure_damage="+ str(i.figure_damage)
            string=string+"\n"
        return string

    def move_troop_f(self,troop,position):
        #leaves current pyramid if any
        if mpyramid.Pyramids.game_pyramids.get_pyramid_f(troop.position)!=None:
            mpyramid.Pyramids.game_pyramids.conquer_f(troop.position)
        for i in self.troops:
            if i.position==troop.position:
                #takes current pyramid if any
                if mpyramid.Pyramids.game_pyramids.get_pyramid_f(position)!=None:
                    mpyramid.Pyramids.game_pyramids.conquer_f(position,conqueror=troop.player.name)
                i.position=position
                return None
        if position==troop.position:
            self.troops.append(troop)

    def choose_retreat_sacrifice_f(self,position,attacker_position ,troop,rival,info):
        if troop.quantity<1:
            self.remove_troop_f(troop)
            return
        are_free=[]
        for i in self.adjacent[position-1]:
            if self.is_free_f(i):
                are_free.append(i)
        if attacker_position!=position and attacker_position in self.adjacent[position-1]:
            are_free.append(attacker_position )
        if len(are_free)==0:
            mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" sacrified in "+str(position)+" \t\t")
            self.sacrifice_f(troop)
            return
        if troop.player.AI==None:
            decision=curses.wrapper(mcurses.draw_menu_f, validValues=['r','s'], info=info+"\n"+troop.player.get_player_string_f(), title="Loser: Choose to Retreat or to Sacrifice your troop")
        else:
            if troop.quantity<3 and troop.figure=="":
                decision='s'
            elif troop.quantity<2:
                decision='s'
            else:
                decision='n'
        if decision=='s':
            mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" sacrified in "+str(position)+" \t\t")
            self.sacrifice_f(troop)
        else:
            if len(are_free)==1:
                mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" retreated "+str(position)+" -> "+str(are_free[0])+" \t\t")
                troop.position=are_free[0]
            else:
                shuffle(are_free)
                for i in are_free:
                    if rival.player.AI!=None:
                        mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" retreated "+str(position)+" -> "+str(i)+" \t\t")
                        troop.position=i
                        return
                    else:
                        decision=curses.wrapper(mcurses.draw_menu_f, validValues=['y','n'], info=info, title="Winner: Retreat the loser to "+str(i)+"?")
                        if decision=='y':
                            mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" retreated "+str(position)+" -> "+str(i)+" \t\t")
                            troop.position=i
                            return
                #if player chooses No for all the adjacent free options
                mturn_history.change_f(mturn_history.string+" \t"+troop.player.name+" retreated "+str(position)+" -> "+str(are_free[0])+" \t\t")
                troop.position=are_free[0]

    def sacrifice_f(self,troop):
        troop.player.prayer.change_f(troop.quantity)
        troop.change_f(-troop.quantity)
        self.remove_troop_f(troop)

    def remove_troop_f(self,troop):
        index=0
        for i in self.troops:
            if i.position==troop.position and i.player==troop.player: #in case recruiting in conquered pyramid
                i.quantity=0
                if i.figure!="":
                    i.player.figures_free.append(i.figure)
                self.troops.pop(index)
                return None
            index+=1
        return 1

    def choose_combat_cards_f(self, troop, status_string, rol, rival, mine, rival_card=None,):
            DIcards=[]
            if troop.player.AI==None:
                if rival_card==None:
                    rival_string=""
                else:
                    rival_string="\nYour rival has chosen the next card:\n"+"\tStrength="+str(rival_card.strength)+" \tDefense="+str(rival_card.defense)+" \tDamage="+str(rival_card.damage)+"\n"
                card_position=int(curses.wrapper(mcurses.draw_menu_f, info=status_string+mBcards.list_cards_string_f(troop.player.Bcards)+rival_string, validValues=mBcards.validValues_f(troop.player.Bcards), title="Choose the position among the available cards ("+rol+") for the card to USE", options_2="the position range starts at 0"))
                troop_card=troop.player.Bcards[card_position]
                troop_card.use_f(troop.player.Bcards)
                cards_to_discard=mBcards.validValues_f(troop.player.Bcards)
                if len(cards_to_discard)==1:
                    troop_card_discard=mBcards.get_cards_f(troop.player.Bcards)[0]
                else:
                    card_position=int(curses.wrapper(mcurses.draw_menu_f, info=status_string+mBcards.list_cards_string_f(troop.player.Bcards)+rival_string, validValues=cards_to_discard, title="Choose the position among the available cards ("+rol+") for the card to DISCARD", options_2="the position range starts at 0"))
                    troop_card_discard=troop.player.Bcards[card_position]
                troop_card_discard.use_f(troop.player.Bcards)
                DIcards_all=troop.player.list_DIcards_f(kind="battle")
                if len(DIcards_all)>0:
                    ok=curses.wrapper(mcurses.draw_menu_f, info=status_string+"\n\nYour DI cards are:\n\t"+", ".join(DIcards_all), validValues=['y','n'], title=rol+": Do you want to use DI cards for this battle?", options_2="for [Yes, No]")
                    if ok=='y':
                        if len(DIcards_all)==1:
                            DIcards=DIcards_all
                        else:
                            for i in DIcards_all:
                                if curses.wrapper(mcurses.draw_menu_f, info=status_string+"\n\nYour DI cards are:\n\t"+", ".join(DIcards_all), validValues=['y','n'], title="Use "+i+"?", options_2="for [Yes, No]")=='y':
                                    DIcards.append(i)
            else:
                DIcards_all=troop.player.list_DIcards_f(kind="battle")
                if len(DIcards_all)>0:
                    shuffle(DIcards_all)
                    if troop.player.AI.choose_f(0,100)>75-len(DIcards_all)*5:
                        for i in DIcards_all:
                            if troop.player.AI.choose_f(0,100)>30+len(DIcards)*30:
                                DIcards.append(i)
                if rival_card==None:
                    rival_strength=rival[0] + troop.player.AI.choose_f(1,3)
                    rival_defense= rival[1] + troop.player.AI.choose_f(0,1)
                    rival_damage=  rival[2] + troop.player.AI.choose_f(0,2)
                else:
                    rival_strength=rival[0] + rival_card.strength
                    rival_defense= rival[1] + rival_card.defense
                    rival_damage=  rival[2] + rival_card.damage
                mine_strength=mine[0]
                mine_defense= mine[1]
                mine_damage=  mine[2]
                if rol!="defender":
                    rival_strength+=1

                troop_card=None
                #try to win           #0.4 or try to get prayer          #try to defend         #if None available then just get a random card
                if rol=="attacker" or (rol=="defender" and troop.player.has_tile_f('Defensive_victory')) or (troop.position in troop.player.city and troop.player.AI.choose_f(1,10)>7):
                    if troop.player.AI.choose_f(1,10)>6:
                        troop_card=mBcards.get_max_card_f(troop.player.Bcards,troop.player, "strength")
                    else:
                        troop_card=mBcards.get_card_f(troop.player.Bcards,troop.player, strength=rival_strength-mine_strength, defense=rival_damage-mine_defense)
                if troop_card==None and ((troop.player.has_tile_f('Crusade') and troop.player.prayer.value<8) or troop.player.AI.choose_f(1,10)>5):
                    if troop.player.AI.choose_f(1,10)>6:
                        troop_card=mBcards.get_max_card_f(troop.player.Bcards,troop.player, "damage")
                    else:
                        troop_card=mBcards.get_card_f(troop.player.Bcards,troop.player, damage=rival_defense-mine_damage)
                if troop_card==None:
                    if troop.player.AI.choose_f(1,10)>6:
                        troop_card=mBcards.get_max_card_f(troop.player.Bcards,troop.player, "defense")
                    else:
                        troop_card=mBcards.get_card_f(troop.player.Bcards,troop.player, defense=rival_damage-mine_defense)
                if troop_card==None:
                    troop_card=mBcards.get_card_f(troop.player.Bcards,troop.player)
                troop_card.use_f(troop.player.Bcards)
                troop_card_discard=mBcards.get_card_f(troop.player.Bcards,troop.player)
                troop_card_discard.use_f(troop.player.Bcards)
            return troop_card,DIcards


    def attack_f(self,attacker,position):
        defender=None
        for i in self.troops:
            if i.position==position:
                defender=i
                break
        if defender==None:
            return 5
        if attacker.player.AI==None or defender.player.AI==None:
            human_player=True
        else:
            human_player=False
        string_battle_result="Attack result on position "+str(position)+":\n"
        tiles_active_string="\n\nTiles (except figures or max/min) active in this attack:\n"
        mverbose.verbose_f(mverbose.Verbose(),"info",attacker.player.name,"attacks from",attacker.position,"to",position)

        #attacker leaves current pyramid if any
        if mpyramid.Pyramids.game_pyramids.get_pyramid_f(attacker.position)!=None:
            mpyramid.Pyramids.game_pyramids.conquer_f(attacker.position)

        if defender.player==attacker.player:
            move_quantity=mplayer.Troop.max_quantity_f(attacker.player, troop=defender)
            if move_quantity==0:
                return 6
            defender.change_f(move_quantity)
            attacker.change_f(-move_quantity)
            if attacker.quantity<1:
                self.remove_troop_f(attacker)
            return None

        escape=False
        if defender.player.has_DIcard_f('Escape'):
            are_free=[]
            for i in self.adjacent[defender.position-1]:
                if self.is_free_f(i) and i!=3:
                    are_free.append(i)
            if attacker.position!=defender.position and attacker.position in self.adjacent[defender.position-1]:
                are_free.append(attacker.position)
            if len(are_free)>0 and ((defender.player.AI!=None and defender.player.AI.choose_f(0,100)>15+defender.quantity*20) or (defender.player.AI==None and curses.wrapper(mcurses.draw_menu_f, info="Attack on position "+str(position)+":\nAttacker from "+str(attacker.position)+" has "+str(attacker.quantity)+" units, while you have "+str(defender.quantity)+"\n\n"+Board.game_board.get_troops_string_f(player=attacker.player)+Board.game_board.get_troops_string_f(player=defender.player), validValues=['y','n'], title="Do you want to use your Escape DIcard?", options_2="for [Yes, No]")=='y')):
                defender.player.discard_DIcard_f('Escape')
                escape=True
                if len(are_free)==1:
                    mturn_history.change_f(mturn_history.string+" \t"+defender.player.name+" retreated "+str(defender.position)+" -> "+str(are_free[0])+" \t\t")
                    defender.position=are_free[0]
                else:
                    shuffle(are_free)
                    for i in are_free:
                        if defender.player.AI!=None:
                            mturn_history.change_f(mturn_history.string+" \t"+defender.player.name+" retreated "+str(defender.position)+" -> "+str(i)+" \t\t")
                            defender.position=i
                            break
                        else:
                            decision=curses.wrapper(mcurses.draw_menu_f, validValues=['y','n'], info="Retreating with Escape in position "+str(defender.position)+"\n\n"+Board.game_board.get_troops_string_f(), title="Retreat to "+str(i)+"?")
                            if decision=='y':
                                mturn_history.change_f(mturn_history.string+" \t"+defender.player.name+" retreated "+str(defender.position)+" -> "+str(i)+" \t\t")
                                defender.position=i
                                break
                    #if player chooses No for all the adjacent free options
                    if defender.position==position:
                        mturn_history.change_f(mturn_history.string+" \t"+defender.player.name+" retreated "+str(defender.position)+" -> "+str(are_free[0])+" \t\t")
                        defender.position=are_free[0]

        #CALC VALUES
        if not escape and attacker.player.has_tile_f('Initiative'):
            defender.change_f(-2)
            tiles_active_string=tiles_active_string+" \tInitiative (attacker),"
        if not escape and defender.quantity>0:
            attacker_strength=attacker.quantity
            defender_strength=defender.quantity
            if attacker.figure_nullify:
                defender_defense=0
                defender_damage=0
            else:
                defender_strength+=defender.figure_strength
                defender_defense=defender.figure_defense
                defender_damage=defender.figure_damage

            if defender.figure_nullify:
                attacker_defense=0
                attacker_damage=0
            else:
                attacker_strength+=attacker.figure_strength
                attacker_defense=attacker.figure_defense
                attacker_damage=attacker.figure_damage

            if attacker.player.has_tile_f('Charge'):
                tiles_active_string=tiles_active_string+" \tCharge (attacker),"
                attacker_strength+=1
            if attacker.player.has_tile_f('Blades_of_Neith'):
                tiles_active_string=tiles_active_string+" \tBlades_of_Neith (attacker),"
                attacker_strength+=1
            if attacker.player.has_tile_f('Carnage'):
                tiles_active_string=tiles_active_string+" \tCarnage (attacker),"
                attacker_damage+=1
            if attacker.player.has_tile_f('Shield_of_Neith'):
                tiles_active_string=tiles_active_string+" \tShield_of_Neith (attacker)"
                attacker_defense+=1

            tiles_active_string=tiles_active_string+"\n"
            if defender.player.has_tile_f('Defense'):
                tiles_active_string=tiles_active_string+" \tDefense (defender),"
                defender_strength+=1
            if defender.player.has_tile_f('Blades_of_Neith'):
                tiles_active_string=tiles_active_string+" \tBlades_of_Neith (defender),"
                defender_strength+=1
            if defender.player.has_tile_f('Carnage'):
                tiles_active_string=tiles_active_string+" \tCarnage (defender),"
                defender_damage+=1
            if defender.player.has_tile_f('Shield_of_Neith'):
                tiles_active_string=tiles_active_string+" \tShield_of_Neith (defender)"
                defender_defense+=1
            tiles_active_string=tiles_active_string+"\n"

            #COMBAT and DI CARDS
            status_string=              "Info on attacker "+attacker.player.name+" from "+str(attacker.position)+" right now:\n\tStrength="+str(attacker_strength)+"\n\tDefense="+str(attacker_defense)+"\n\tDamage="+str(attacker_damage)+"\n\t"+attacker.player.get_player_string_f()+"\n\n"
            status_string=status_string+"Info on defender "+defender.player.name+" from "+str(defender.position)+" right now:\n\tStrength="+str(defender_strength)+"\n\tDefense="+str(defender_defense)+"\n\tDamage="+str(defender_damage)+"\n\t"+defender.player.get_player_string_f()+"\n\n"

            status_string_2=status_string+Board.game_board.get_troops_string_f()+"\n\n"
            attacker_array=[attacker_strength,attacker_defense,attacker_damage]
            defender_array=[defender_strength,defender_defense,defender_damage]
            if attacker.player.has_tile_f('Prescience'):
                defender_card,defender_DIcards=self.choose_combat_cards_f(defender,status_string_2,"defender", attacker_array, defender_array)
                attacker_card,attacker_DIcards=self.choose_combat_cards_f(attacker,status_string_2,"attacker", defender_array, attacker_array, rival_card=defender_card)
            elif defender.player.has_tile_f('Prescience'):
                attacker_card,attacker_DIcards=self.choose_combat_cards_f(attacker,status_string_2,"attacker", defender_array, attacker_array)
                defender_card,defender_DIcards=self.choose_combat_cards_f(defender,status_string_2,"defender", attacker_array, defender_array, rival_card=attacker_card)
            else:
                attacker_card,attacker_DIcards=self.choose_combat_cards_f(attacker,status_string_2,"attacker", defender_array, attacker_array)
                defender_card,defender_DIcards=self.choose_combat_cards_f(defender,status_string_2,"defender", attacker_array, defender_array)

            for i in attacker_DIcards:
                if attacker.player.discard_DIcard_f(i)==None:
                    if i=='War_rage':
                        attacker_strength+=1
                    elif i=='War_fury':
                        attacker_strength+=2
                    elif i=='Bloody_battle':
                        attacker_damage+=1
                    elif i=='Bloodbath':
                        attacker_damage+=2
                    elif i=='Bronze_wall':
                        attacker_defense+=1
                    elif i=='Iron_wall':
                        attacker_defense+=2
            for i in defender_DIcards:
                if defender.player.discard_DIcard_f(i)==None:
                    if i=='War_rage':
                        defender_strength+=1
                    elif i=='War_fury':
                        defender_strength+=2
                    elif i=='Bloody_battle':
                        defender_damage+=1
                    elif i=='Bloodbath':
                        defender_damage+=2
                    elif i=='Bronze_wall':
                        defender_defense+=1
                    elif i=='Iron_wall':
                        defender_defense+=2

            attacker_strength+=attacker_card.strength
            attacker_defense+=attacker_card.defense
            attacker_damage+=attacker_card.damage
            defender_strength+=defender_card.strength
            defender_defense+=defender_card.defense
            defender_damage+=defender_card.damage

            status_string=              "Info on attacker "+attacker.player.name+" right now:\n\tStrength="+str(attacker_strength)+"\n\tDefense="+str(attacker_defense)+"\n\tDamage="+str(attacker_damage)+"\n\n"
            status_string=status_string+"Info on defender "+defender.player.name+" right now:\n\tStrength="+str(defender_strength)+"\n\tDefense="+str(defender_defense)+"\n\tDamage="+str(defender_damage)+"\n\n"
            if attacker.player.has_tile_f('Divine_wound') and attacker_strength<=defender_strength and attacker_strength+len(attacker.player.DIcards)>defender_strength:
                gap=defender_strength-attacker_strength
                if attacker.player.AI==None:
                    ok=curses.wrapper(mcurses.draw_menu_f, info=status_string+"\n\nYour DI cards are:\n\t"+", ".join(attacker.player.list_DIcards_f()), validValues=['y','n'], title="Do you want to discard any DI card to gain 1 strength for each?", options_2="for [Yes, No]")
                    if ok=='y':
                        #TODO seleccionar manualmente
                        attacker.player.discard_DIcards_f(gap)
                        attacker_strength+=gap
                elif attacker.player.AI.choose_f(0,100)>65+gap*10:
                    attacker.player.discard_DIcards_f(gap)
                    attacker_strength+=gap
            elif defender.player.has_tile_f('Divine_wound') and defender_strength<attacker_strength and defender_strength+len(defender.player.DIcards)>=attacker_strength:
                gap=attacker_strength-defender_strength
                if defender.player.AI==None:
                    ok=curses.wrapper(mcurses.draw_menu_f, info=status_string+"\n\nYour DI cards are:\n\t"+", ".join(defender.player.list_DIcards_f()), validValues=['y','n'], title="Do you want to discard any DI card to gain 1 strength for each?", options_2="for [Yes, No]")
                    if ok=='y':
                        #TODO seleccionar manualmente
                        defender.player.discard_DIcards_f(gap)
                        defender_strength+=gap
                elif defender.player.AI.choose_f(0,100)>65+gap*10:
                    defender.player.discard_DIcards_f(gap)
                    defender_strength+=gap

        else:
            string_battle_result=string_battle_result+"\nThe defender was destroyed before the battle, so there is no battle\n\n"
            attacker_strength=1
            attacker_defense=0
            attacker_damage=0
            attacker_loses=0
            defender_strength=0
            defender_defense=0
            defender_damage=0
            defender_loses=0

        if escape:
            string_battle_result=string_battle_result+"\nThe defender used his Escape DI card to "+str(defender.position)+" before the battle, so there is no battle\n\n"
        else:
            #DAMAGE TO TROOPS
            if attacker_damage>defender_defense:
                defender_loses=min(attacker_damage-defender_defense, defender.quantity)
                defender.change_f(-defender_loses)
            else:
                defender_loses=0
            if defender_damage>attacker_defense:
                attacker_loses=min(defender_damage-attacker_defense, attacker.quantity)
                attacker.change_f(-attacker_loses)
            else:
                attacker_loses=0
            if attacker.player.has_tile_f('Crusade'):
                tiles_active_string=tiles_active_string+" \tCrusade (attacker)"
                attacker.player.prayer.change_f(2*defender_loses)
            elif defender.player.has_tile_f('Crusade'):
                tiles_active_string=tiles_active_string+" \tCrusade (defender)"
                defender.player.prayer.change_f(2*attacker_loses)

        string_battle_result=string_battle_result+\
            "\tAttacker "+attacker.player.name+ "\tattacker_strength="+str(attacker_strength)+ " \tattacker_defense="+str(attacker_defense)+ " \tattacker_damage="+str(attacker_damage)+" \tatacker_loses="+str(attacker_loses)+" \tfinal attacker quantity="+str(attacker.quantity)+"\n"+\
            "\tDefender "+defender.player.name+ "\tdefender_strength="+str(defender_strength)+ "\tdefender_defense="+str(defender_defense)+ "\tdefender_damage="+str(defender_damage)+" \tdefender_loses="+str(defender_loses)+" \tfinal defender quantity="+str(defender.quantity)+"\n"

        if escape:
            attacker.position=position
        else:
            #WINNER: if attacker wins and no troops left defender can stay, but if defender wins and has no troops the attacker retreats or sacrifices (checked online)
            winner=None
            if attacker_strength>defender_strength:
                if attacker.quantity>0:
                    winner=attacker
                    attacker.position=position
                    attacker.player.battle_points+=1
                    if not escape:
                        self.choose_retreat_sacrifice_f(position,attacker.position,defender,attacker,string_battle_result+"\n"+Board.game_board.get_troops_string_f())
                else:
                    self.remove_troop_f(attacker)
                    if defender.quantity<1:
                        self.remove_troop_f(defender)
            else:
                if defender.quantity>0:
                    winner=defender
                    if defender.player.has_tile_f('Defensive_victory'):
                        tiles_active_string=tiles_active_string+" \tDefensive_victory (defender)"
                        defender.player.battle_points+=1
                else:
                    self.remove_troop_f(defender)
                if not escape:
                    self.choose_retreat_sacrifice_f(position,attacker.position,attacker,defender,string_battle_result+"\n"+Board.game_board.get_troops_string_f())

            if winner!=None and winner.player.has_tile_f('Holy_war'):
                tiles_active_string=tiles_active_string+" \tHoly_war (winner)"
                winner.player.prayer.change_f(4)

        if attacker.quantity>0 and mpyramid.Pyramids.game_pyramids.get_pyramid_f(attacker.position)!=None:
            mpyramid.Pyramids.game_pyramids.conquer_f(attacker.position, conqueror=attacker.player.name)

        if defender.quantity>0 and mpyramid.Pyramids.game_pyramids.get_pyramid_f(defender.position)!=None:
            mpyramid.Pyramids.game_pyramids.conquer_f(defender.position, conqueror=defender.player.name)


        if human_player:
            if winner==None:
                if escape:
                    string_battle_result=string_battle_result+"\tWinner: NONE, because Escape DIcard was used"
                else:
                    string_battle_result=string_battle_result+"\tWinner: NONE, because there were no troops remainig for him at the end"
            else:
                string_battle_result=string_battle_result+"\tWinner: "+winner.player.name
            mverbose.verbose_f(mverbose.Verbose(),"normal",string_battle_result +tiles_active_string)
            curses.wrapper(mcurses.draw_menu_f, info=string_battle_result +tiles_active_string, title="Informative battle results")

    def get_paths_f(self,ori_list,dest,distance,troop):
        temp=[]
        paths=[]
        for ori in ori_list:
            ori_troop=self.get_troop_f(ori)
            if ori_troop!=None and ori_troop.player!=troop.player and ori!=dest:      #teleporting check
                continue
            if distance==1:
                if dest in self.adjacent[ori-1]:
                    return [[ori,dest]]
            else:
                if distance>=2:
                    if dest in self.adjacent[ori-1]:
                        temp.append([[ori,dest]])
                    for i in self.paths.len_3:
                        if i[0]==ori and i[2]==dest and self.is_free_f(i[1]):
                            temp.append(i)
                if distance>=3:
                    for i in self.paths.len_4:
                        if i[0]==ori and i[3]==dest and self.is_free_f(i[1]) and self.is_free_f(i[2]):
                            temp.append(i)
                if distance==4:
                    for i in self.paths.len_5:
                        if i[0]==ori and i[4]==dest and self.is_free_f(i[1]) and self.is_free_f(i[2]) and self.is_free_f(i[3]):
                            temp.append(i)
        #WALL CHECK
        for i in temp:
            if (ori in self.wall[0] and dest in self.wall[0]) or (ori in self.wall[1] and dest in self.wall[1]) or \
            (dest not in self.wall[0] and dest not in self.wall[1] and dest not in self.wall[2] and dest not in self.wall[3]) or \
            (ori in self.wall[2] and dest in self.wall[2]) or (ori in self.wall[3] and dest in self.wall[3]) or \
            troop.player.has_tile_f('Open_Gates') or troop.figure=='Phoenix' or troop.player.has_DIcard_f('Open_gates') or \
            dest==self.wall[0][0] or dest==self.wall[1][0] or dest==self.wall[2][0] or dest==self.wall[3][0] or dest in troop.player.city:
                paths.append(i)

        return paths

    def can_reach_f(self,dest,troop):
        #WALKING MOVEMENTS
        ori=troop.position
        if dest>self.map_max_position or ori==dest:
            return False
        distance=troop.distance
        if troop.player.has_tile_f('God_Speed'):
            distance+=1
        paths=self.get_paths_f([ori],dest,distance,troop)
        cost=0
        #OBELISK MOVEMENTS
        if len(paths)==0 and (ori in self.pyramid or (ori in self.obelisk and troop.player.has_tile_f('Teleport'))):
            paths=self.get_paths_f(self.obelisk,dest,distance,troop)
            cost=2
            if troop.player.has_tile_f('Stargate'):
                cost-=1
            if troop.player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0
        #Teleportation DIcard for 1 prayer, in which case ORI DOES NOT MATTER (checked online)
        if len(paths)==0 and troop.player.has_DIcard_f('Teleportation') and troop.player.prayer.value>=1:
            paths=self.get_paths_f(self.obelisk,dest,distance,troop)
            cost=1
            if troop.player.has_tile_f('Stargate'):
                cost-=1
            if troop.player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0
        if len(paths)==0:
            return False
        if troop.player.prayer.value<cost:
            return False
        return True

    def move_f(self,dest,troop):
        #WALKING MOVEMENTS
        ori=troop.position
        if dest>self.map_max_position:
            return 1
        if ori==dest:
            return 2
        distance=troop.distance
        if troop.player.has_tile_f('God_Speed'):
            distance+=1
        paths=self.get_paths_f([ori],dest,distance,troop)
        cost=0
        #OBELISK MOVEMENTS
        if len(paths)==0 and (ori in self.pyramid or (ori in self.obelisk and troop.player.has_tile_f('Teleport'))):
            paths=self.get_paths_f(self.obelisk,dest,distance,troop)
            cost=2
            if troop.player.has_tile_f('Stargate'):
                cost-=1
            if troop.player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0

        need_open_gates=False
        #if too away and: from no teleportation place (len 0) or not enough prayer to teleport (len 1)
        #Teleportation DIcard for 1 prayer, in which case ORI DOES NOT MATTER (checked online)
        if (len(paths)==0 or (len(paths)==1 and troop.player.prayer.value<cost)) and troop.player.has_DIcard_f('Teleportation'):
            paths=self.get_paths_f(self.obelisk,dest,distance,troop)
            if len(paths)>0:
                if troop.player.discard_DIcard_f('Teleportation')!=None:
                    return 4
            cost=1
            if troop.player.has_tile_f('Stargate'):
                cost-=1
            if troop.player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0
        else:
            if len(paths)==0:
                return 5
            for i in self.wall:
                if dest in i:
                    wall=i
                    if ori in wall or troop.player.has_tile_f('Open_Gates') or troop.figure=="Phoenix" or \
                    dest==self.wall[0][0] or dest==self.wall[1][0] or dest==self.wall[2][0] or dest==self.wall[3][0] or dest in troop.player.city:
                        need_open_gates=False
                    else:
                        need_open_gates=True
                    break
        if need_open_gates:
            cost+=1
        if troop.player.prayer.value<cost:
            return 6
        if need_open_gates:
            if troop.player.has_DIcard_f('Open_gates'):
                if troop.player.discard_DIcard_f('Open_gates')!=None:
                    return 7
            else:
                return 8
        troop.player.prayer.change_f(-cost)
        if self.is_free_f(dest):
            return self.move_troop_f(troop,dest)
        else:
            return self.attack_f(troop,dest)

