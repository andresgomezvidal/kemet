import mplayer
from random import shuffle
import mcurses, curses

class Battle_card:
    def __init__(self,strength,defense,damage):
        self.strength=strength
        self.defense=defense
        self.damage=damage
        self.used=False

    def use_f(self,cards):
        self.used=True
        for i in cards:
            if i.used==False:
                return
        for i in cards:
            i.used=False


def initial_cards_f():
    cards=[]
    cards.append(Battle_card(2,2,0))
    cards.append(Battle_card(3,1,0))
    cards.append(Battle_card(1,0,3))
    cards.append(Battle_card(2,1,2))
    cards.append(Battle_card(3,0,2))
    cards.append(Battle_card(4,0,1))
    return cards

def list_cards_string_f(cards,used=False):
    available=get_cards_f(cards,used=used)
    string="Listing available cards:\n"
    for i in available:
        string=string+"\tStrength="+str(i.strength)+" \tDefense="+str(i.defense)+" \tDamage="+str(i.damage)+"\n"
    return string

def get_cards_f(cards,used=False):
    available=[]
    for i in cards:
        if i.used==used:
            available.append(i)
    return available

def get_max_card_f(cards,player,kind):
    if kind!="strength" and kind!="defense" and kind!="damage":
        return get_card_f()
    available=get_cards_f(cards)
    max_card=available[0]
    for i in available:
        if kind=="strength":
            if max_card.strength<i.strength:
                max_card=i
        elif kind=="strength":
            if max_card.defense<i.defense:
                max_card=i
        elif kind=="damage":
            if max_card.damage<i.damage:
                max_card=i
    return max_card


#return available card with at least those parameters or a random card
def get_card_f(cards,player,strength=None,defense=None,damage=None):
    if strength==None and defense==None and damage==None:
        cards_shuffled=list(cards)
        shuffle(cards_shuffled)
        for i in cards_shuffled:
            if i.used==False:
                return i
    filtered=[]
    available=get_cards_f(cards)
    for i in available:
        if strength!=None:
            if strength<=i.strength:
                continue
        if defense!=None:
            if defense<=i.defense:
                continue
        if damage!=None:
            if damage<=i.damage:
                continue
        filtered.append(i)

    if len(filtered)==0:
        if player.AI!=None:
            return None
        string_2=":"
        if strength!=None:
            string_2=string_2+" \tstrength="+str(strength)
        if defense!=None:
            string_2=string_2+" \tdefense="+str(defense)
        if damage!=None:
            string_2=string_2+" \tdamage="+str(damage)
        card_position=int(curses.wrapper(mcurses.draw_menu_f, info="There is no card available with those paramets"+string_2+list_cards_string_f(cards), validValues=validValues_f(cards), title="Choose the position among the available cards", options_2="the position range starts at 0"))
        return cards[card_position]
    elif len(filtered)==1:
        return filtered[0]
    else:
        if player.AI!=None:
            shuffle(filtered)
            return filtered[0]
        string_2=":"
        if strength!=None:
            string_2=string_2+" \tstrength="+str(strength)
        if defense!=None:
            string_2=string_2+" \tdefense="+str(defense)
        if damage!=None:
            string_2=string_2+" \tdamage="+str(damage)
        card_position=int(curses.wrapper(mcurses.draw_menu_f, info="There are too many cards available with those paramets"+string_2+list_cards_string_f(filtered), validValues=validValues_f(filtered), title="Choose the position among the filtered cards", options_2="the position range starts at 0"))
        return cards[card_position]

def validValues_f(cards,used=False):
    validValues=[]
    for i in range(0,len(cards)):
        if cards[i].used==used:
            validValues.append(i)
    return validValues

def exchange_cards_f(player,cards,color):
    if color=="red":
        card_after=Battle_card(3,0,3)
    elif color=="blue":
        card_after=Battle_card(3,3,0)
    else:
        return
    for i in cards:
        i.used=False
    if player.AI==None:
        card_position=int(curses.wrapper(mcurses.draw_menu_f, info=list_cards_string_f(cards), validValues=validValues_f(cards), title="Choose the position among the cards to be traded with the special "+color+" card"))
        card_before=cards[card_position]
    else:
        power4=[]
        for i in cards:
            if i.strength+i.defense+i.damage==4:
                power4.append(i)
        shuffle(power4)
        card_before=power4[0]
    for i in range(0,len(cards)):
        if cards[i]==card_before:
            cards.pop(i)
            cards.append(card_after)
            return

