import mplayer
import mboard
import mpyramid
import mtile
import mDIcards
import mverbose
import mturn_history
import mcurses, curses
from random import shuffle

#test crashes: while true; do python start.py || break; done
# while true; do python start.py |tail|awk '/The game has finished in/ {print $6}'; done
# awk '{total+=$1; lines++} END{print total/lines}'

#TODO main: maybe chance to choose a pyramid to buy/level around, 13 units bug (range?), leave troops in path if not full and not empty troop moving
# points check not getting conquered pyramids

#Hand_of_God 1 pyramid not upgraded


game_players_sorted=list(mplayer.Player.game_players)
shuffle(game_players_sorted)

turn=1
init_AI=True
while turn<25:
    mplayer.Player.game_players=game_players_sorted
    current_player_index=0
    attempts=[]
    while current_player_index<len(game_players_sorted):
        action_result=None
        current_player=game_players_sorted[current_player_index]
        if len(attempts)>=30:               #locked
            for z in attempts:
                for x in attempts:
                    if z!=x and attempts.count(z)>attempts.count(x):
                        index_z=attempts.index(z)
                        index_x=attempts.index(x)
                        if index_z>index_x:
                            temp=x
                            attempts[index_x]=z
                            attempts[index_z]=temp
            for z in attempts:
                mverbose.verbose_f(mverbose.Verbose(),"error", "Too many attempts of not found action '"+z+"' from",current_player.name,"in turn",turn)
                if z in current_player.actions_available:
                    current_player.actions_available.pop(current_player.actions_available.index(z))
                elif z=="buy":
                    if "red" in current_player.actions_available:
                        current_player.actions_available.pop(current_player.actions_available.index("red"))
                    if "blue" in current_player.actions_available:
                        current_player.actions_available.pop(current_player.actions_available.index("blue"))
                    if "white" in current_player.actions_available:
                        current_player.actions_available.pop(current_player.actions_available.index("white"))
                else:       #final else (can do nothing)
                    current_player.actions_available=[]
                break
            init_AI=True
            current_player.actions+=1
            current_player_index+=1
            attempts=[]
            continue


        #PLAYER PART
        if current_player.player_have_actions_left_f() and current_player.AI==None:
            info=mboard.Board.game_board.get_troops_string_f()+mpyramid.Pyramids.game_pyramids.list_pyramids_string_f()+mplayer.get_players_string_f()+mturn_history.string
            if current_player.actions<current_player.actions_max:
                validActions=current_player.get_validValues_actions_f("actions")
                if 'g' in validActions:
                    gold_string=", Gold"
                else:
                    gold_string=""
                action=curses.wrapper(mcurses.draw_menu_f, validValues=validActions, title="Action for "+current_player.name, player=current_player, options_2="for available actions among [Buy, Levelup, Pray, Move, Recruit"+gold_string+"]", info=info)
            else:
                action='g'
            if action=='g':
                gold_sub=curses.wrapper(mcurses.draw_menu_f, validValues=['m','r'], title="Gold action: choose to Recruit or Move", player=current_player)
            else:
                gold_sub=''
            #BUY
            if action=='b':
                color=curses.wrapper(mcurses.draw_menu_f, validValues=current_player.get_validValues_actions_f("buy_color"), title="Color of the tile to buy", player=current_player, options_2="for actions [Red, Blue, White]", info=info)
                if color=='r':
                    color="red"
                elif color=='b':
                    color="blue"
                elif color=='w':
                    color="white"
                elif color=='q':
                    continue
                tiles=mtile.GameTiles.game_tiles.get_tiles_f(color=color, cost=-mpyramid.Pyramids.game_pyramids.max_level_f(color=color,name=current_player.name), quantity=-1)
                if len(tiles)==0:
                    continue
                validValues=list(v.__class__.__name__ for v in tiles)
                string=curses.wrapper(mcurses.draw_menu_f, validValues=validValues, title="Name of the tile to buy", player=current_player, info=mtile.GameTiles.game_tiles.list_names_available_string_f(tiles=tiles)+"\n"+current_player.get_player_string_f())
                for z in validValues:
                    if z.lower()==string:
                        string=z
                        break
                action_result=current_player.action_f("buy", string=string)
                if action_result!=None:
                    continue
                else:
                    turn_string="\t"+current_player.name+" bought tile "+string+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #LEVELUP
            elif action=='l':
                pyramids=mpyramid.Pyramids.game_pyramids.get_pyramids_f(owner=current_player.name,level=-4)
                position=curses.wrapper(mcurses.draw_menu_f, validValues=list(v.position for v in pyramids), title="Position of the pyramid to upgrade", player=current_player, info=info)
                if position=='q':
                    continue
                position=int(position)
                pyramid=mpyramid.Pyramids.game_pyramids.get_pyramid_f(position)
                if pyramid==None or pyramid.level==4:
                    continue
                nlevel=curses.wrapper(mcurses.draw_menu_f, validValues=list(range(pyramid.level+1,5)), title="Level of the pyramid in position "+str(position)+" to be upgraded to", player=current_player, info=info)
                if nlevel=='q':
                    continue
                nlevel=int(nlevel)
                action_result=current_player.action_f("levelup", nlevel=nlevel, position=position)
                if action_result!=None:
                    continue
                else:
                    turn_string="\t"+current_player.name+" upgraded the "+mpyramid.Pyramids.game_pyramids.get_pyramid_f(position).color+" pyramid in position "+str(position)+" to level "+str(nlevel)+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #PRAY
            elif action=='p':
                action_result=current_player.action_f("pray")
                if action_result!=None:
                    continue
                else:
                    turn_string="\t"+current_player.name+" prayed\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #MOVE
            elif action=='m' or gold_sub=='m':
                troop_position=curses.wrapper(mcurses.draw_menu_f, validValues=list(v.position for v in mboard.Board.game_board.get_troops_f(player=current_player)), title="Position of the troop to move", player=current_player, info=info)
                if troop_position=='q':
                    continue
                troop_position=int(troop_position)
                troop_dest=curses.wrapper(mcurses.draw_menu_f, validValues=list(range(1,mboard.Board.game_board.map_max_position+1)), title="Position to move the troop from "+str(troop_position)+" to", player=current_player, info=info)
                if troop_dest=='q':
                    continue
                troop_dest=int(troop_dest)
                if action=='m':
                    action_result=current_player.action_f("move", position=troop_position, dest=troop_dest)
                elif action=='g':
                    action_result=current_player.action_f("gold", position=troop_position, dest=troop_dest, gold_sub=gold_sub)
                if action_result!=None:
                    continue
                else:
                    turn_string="\t"+current_player.name+" moved from "+str(troop_position)+" to "+str(troop_dest)+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #RECRUIT
            elif action=='r' or gold_sub=='r':
                recruit_array=[]            #[[quantity,position]]
                quantity_selected=0
                positions_all=list(v.position for v in mpyramid.Pyramids.game_pyramids.get_pyramids_f(creator=current_player.name))
                positions=[]
                troop_quantity_min=1
                if current_player.has_tile_f('Recruiting_scribe'):
                    troop_quantity_min=0
                for v in positions_all:
                    troop_existing=mboard.Board.game_board.get_troop_f(v)
                    if troop_existing!=None:
                        troop_quantity_max=mplayer.Troop.max_quantity_f(current_player, troop=troop_existing, quantity_selected=quantity_selected)
                        if troop_quantity_max==None or troop_quantity_max<troop_quantity_min:
                            continue
                    positions.append(v)

                if len(positions)==0:
                    continue
                if len(positions)==1:
                    troop_quantity_max=mplayer.Troop.max_quantity_f(current_player, troop=mboard.Board.game_board.get_troop_f(positions[0]), quantity_selected=quantity_selected)
                    answer=curses.wrapper(mcurses.draw_menu_f, validValues=list(range(troop_quantity_min,troop_quantity_max+1)), title="Quantity to recruit in "+str(positions[0]), player=current_player, info=info)
                    if answer=='q':
                        continue
                    troop_quantity=int(answer)
                    quantity_selected+=troop_quantity
                    recruit_array.append([troop_quantity, positions[0]])
                else:
                    for v in positions:
                        if quantity_selected>=current_player.troop_left:
                            continue
                        answer=curses.wrapper(mcurses.draw_menu_f, validValues=['y','n'], title="Do you want to recruit in "+str(v)+"?", player=current_player, info=info)
                        if answer=='q':
                            break
                        if answer!='y':
                            continue
                        troop_quantity_max=mplayer.Troop.max_quantity_f(current_player, troop=mboard.Board.game_board.get_troop_f(v), quantity_selected=quantity_selected)
                        if troop_quantity_max==None or troop_quantity_max<troop_quantity_min:
                            continue
                        answer=curses.wrapper(mcurses.draw_menu_f, validValues=list(range(troop_quantity_min,troop_quantity_max+1)), title="Quantity to recruit in "+str(v), player=current_player, info=info)
                        if answer=='q':
                            break
                        troop_quantity=int(answer)
                        quantity_selected+=troop_quantity
                        recruit_array.append([troop_quantity, v])

                if len(recruit_array)==0:
                    continue
                recruit_info=""
                for v in recruit_array:
                    recruit_info=recruit_info+str(v[0])+" in "+str(v[1])+", "
                recruit_info=recruit_info[0:-2]
                ok=curses.wrapper(mcurses.draw_menu_f, info="Recruiting: "+recruit_info, validValues=['y','n'], title="Is this correct?", options_2="for [Yes, No]")
                if ok!='y':
                    continue
                if action=='r':
                    action_result=current_player.action_f("recruit", recruit_array=recruit_array)
                elif action=='g':
                    action_result=current_player.action_f("gold", recruit_array=recruit_array, gold_sub=gold_sub)
                if action_result!=None:
                    continue
                else:
                    for z in recruit_array:
                        turn_string="\t"+current_player.name+" recruited "+recruit_info+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #DIcard
            elif action=='c':
                available_cards=[]
                for z in current_player.DIcards:
                    if 'Raining_fire'==z.__class__.__name__ or 'Prayer'==z.__class__.__name__ or ('Enlistmen'==z.__class__.__name__ and current_player.troop_left>0) or 'Mana_theft'==z.__class__.__name__:
                        available_cards.append(z)
                card=curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f()+mplayer.get_players_string_f()+"\n"+mDIcards.GameDICards.game_DIcards.list_cards_string_f(available_cards), validValues=list(v.__class__.__name__ for v in available_cards), title="Choose among the available cards")
                if card=='q':
                    continue
                current_player.discard_DIcard_f(card)
                continue
            else:
                attempts.append('q')
                mverbose.verbose_f(mverbose.Verbose(),"warning", "There is no action",action," \t",current_player.name," in turn",turn)
                continue




        #AI PART
        elif current_player.player_have_actions_left_f():
            if init_AI:             #just do it once each time (avoid continue overhead)
                init_AI=not init_AI
                validActions=list(current_player.get_validValues_actions_f("actions"))
                if 'm' in validActions or 'g' in validActions:
                    strong,weak=mboard.Board.game_board.get_vulnerable_troop_AI_f(current_player,AI_friendly=mplayer.Player.AI_friendly)
            if current_player.actions>=current_player.actions_max and 'g' in validActions:
                if strong!=None and weak!=None:
                    action='m'
                elif current_player.AI.choose_f(1,100)>current_player.chance_recruit_f(attempts):
                    action='r'
                else:
                    action='m'
                attempts.append('gold')
            else:
                if 'c' in validActions and current_player.AI.choose_f(0,100)>80:
                    action='c'
                    attempts.append('card')
                elif ('m' in validActions or 'g' in validActions) and mboard.Board.game_board.get_troops_f(player=current_player)!=None and current_player.AI.choose_f(0,100)>current_player.chance_move_f(strong,weak,attempts):
                    if 'm' in validActions:
                        attempts.append('move')
                    else:
                        attempts.append('gold')
                    action='m'
                elif 'b' in validActions and current_player.AI.choose_f(0,100)>current_player.chance_buy_f(turn,attempts):
                    action='b'
                    attempts.append('buy')
                elif 'l' in validActions and current_player.AI.choose_f(0,100)>current_player.chance_levelup_f(attempts):
                    action='l'
                    attempts.append('levelup')
                elif ('r' in validActions or 'g' in validActions) and current_player.AI.choose_f(0,100)>current_player.chance_recruit_f(attempts):
                    if 'r' in validActions:
                        attempts.append('recruit')
                    else:
                        attempts.append('gold')
                    action='r'
                elif 'p' in validActions and current_player.AI.choose_f(0,100)>current_player.chance_pray_f(attempts):
                    action='p'
                    attempts.append('pray')
                else:
                    if 'p' in validActions and current_player.prayer.value<11:
                        action='p'
                        attempts.append('pray')
                    else:
                        attempts.append('final else')
                        continue

            #BUY
            if action=='b':
                validValues=list(current_player.get_validValues_actions_f("buy_color"))
                if len(validValues)==0:
                    continue
                shuffle(validValues)
                success=False
                for color in validValues:
                    if color=='r':
                        color="red"
                    elif color=='b':
                        color="blue"
                    elif color=='w':
                        color="white"
                    else:
                        continue
                    tiles=mtile.GameTiles.game_tiles.get_tiles_f(color=color, cost=-mpyramid.Pyramids.game_pyramids.max_level_f(color=color,name=current_player.name), quantity=-1)
                    if len(tiles)==0:
                        continue
                    validNames=list(v.__class__.__name__ for v in tiles)
                    for string in validNames:
                        for v in current_player.tiles.list:
                            if string==v.__class__.__name__:
                                validNames.pop(validNames.index(string))
                    if len(validNames)==0:
                        continue
                    string=None
                    figures_owned=mboard.Board.game_board.count_figures_f(current_player)
                    if "Crusade" in validNames and current_player.AI.choose_f(0,100)>35 and current_player.action_f("buy", string="Crusade")==None:
                        string="Crusade"
                    elif "Defensive_victory" in validNames and current_player.AI.choose_f(0,100)>35 and current_player.action_f("buy", string="Defensive_victory")==None:
                        string=""
                    elif "Legion" in validNames and current_player.AI.choose_f(0,100)>35 and current_player.action_f("buy", string="Legion")==None:
                        string="Legion"
                    elif "Divine_will" in validNames and current_player.AI.choose_f(0,100)>25 and current_player.action_f("buy", string="Divine_will")==None:
                        string="Divine_will"
                    elif "Sphinx " in validNames and current_player.AI.choose_f(0,100)>25-(current_player.permanent_points+current_player.battle_points)*5 and current_player.action_f("buy", string="Sphinx ")==None:
                        string="Sphinx "
                    elif "Victory_Point_red" in validNames and current_player.AI.choose_f(0,100)>45-(current_player.permanent_points+current_player.battle_points)*5 and current_player.action_f("buy", string="Victory_Point_red")==None:
                        string="Victory_Point_red"
                    elif "Victory_Point_blue" in validNames and current_player.AI.choose_f(0,100)>45-(current_player.permanent_points+current_player.battle_points)*5 and current_player.action_f("buy", string="Victory_Point_blue")==None:
                        string="Victory_Point_blue"
                    elif "Victory_Point_white" in validNames and current_player.AI.choose_f(0,100)>45-(current_player.permanent_points+current_player.battle_points)*5 and current_player.action_f("buy", string="Victory_Point_white")==None:
                        string="Victory_Point_white"
                    elif "" in validNames and current_player.AI.choose_f(0,100)>25 and current_player.action_f("buy", string="")==None:
                        string=""
                    elif "Hand_of_God" in validNames and current_player.AI.choose_f(0,100)>45+turn*10 and current_player.action_f("buy", string="Hand_of_God")==None:
                        string="Hand_of_God"
                    elif "Slaves" in validNames and current_player.AI.choose_f(0,100)>35+turn*10 and current_player.action_f("buy", string="Slaves")==None:
                        string="Slaves"
                    elif "Act_of_God_red" in validNames and current_player.AI.choose_f(0,100)>55+(5-current_player.actions_max)*20 and current_player.action_f("buy", string="Act_of_God_red")==None:
                        string="Act_of_God_red"
                    elif "Act_of_God_blue" in validNames and current_player.AI.choose_f(0,100)>55+(5-current_player.actions_max)*20 and current_player.action_f("buy", string="Act_of_God_blue")==None:
                        string="Act_of_God_blue"
                    elif "Act_of_God_white" in validNames and current_player.AI.choose_f(0,100)>55+(5-current_player.actions_max)*20 and current_player.action_f("buy", string="Act_of_God_white")==None:
                        string="Act_of_God_white"
                    elif "Giant_Scorpion" in validNames and (current_player.has_tile_f("Crusade") or current_player.AI.choose_f(0,100)>45+figures_owned*20) and current_player.action_f("buy", string="Giant_Scorpion")==None:
                        string="Giant_Scorpion"
                    elif "Carnage" in validNames and (current_player.has_tile_f("Crusade") or current_player.AI.choose_f(0,100)>75) and current_player.action_f("buy", string="Carnage")==None:
                        string="Carnage"
                    elif "Phoenix" in validNames and ((not current_player.has_tile_f("Open_Gates") and current_player.AI.choose_f(0,100)>70+figures_owned*20) or current_player.AI.choose_f(0,100)>45+figures_owned*20) and current_player.action_f("buy", string="Phoenix")==None:
                        string="Phoenix"
                    elif "Open_Gates" in validNames and current_player.AI.choose_f(0,100)>45-turn*5 and current_player.action_f("buy", string="Open_Gates")==None:
                        string="Open_Gates"
                    elif "Initiative" in validNames and current_player.AI.choose_f(0,100)>15+turn*5 and current_player.action_f("buy", string="Initiative")==None:
                        string="Initiative"
                    elif "God_Speed" in validNames and current_player.AI.choose_f(0,100)>35+turn*10 and current_player.action_f("buy", string="God_Speed")==None:
                        string="God_Speed"
                    elif "Teleport" in validNames and current_player.AI.choose_f(0,100)>75-turn*10 and current_player.action_f("buy", string="Teleport")==None:
                        string="Teleport"
                    elif "Priest" in validNames and current_player.AI.choose_f(0,100)>45+turn*5 and current_player.action_f("buy", string="Priest")==None:
                        string="Priest"
                    elif "Priestess" in validNames and current_player.AI.choose_f(0,100)>45+turn*5 and current_player.action_f("buy", string="Priestess")==None:
                        string="Priestess"
                    elif "Priest_of_Ra" in validNames and current_player.AI.choose_f(0,100)>35+turn*5 and current_player.action_f("buy", string="Priest_of_Ra")==None:
                        string="Priest_of_Ra"


                    if string!=None:
                        turn_string="\t"+current_player.name+" bought tile "+string+"\n"
                        mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                        mturn_history.change_f(mturn_history.string+turn_string)
                        success=True
                    else:
                        shuffle(validNames)
                        for string in validNames:
                            action_result=current_player.action_f("buy", string=string)
                            if action_result!=None:
                                continue
                            else:
                                turn_string="\t"+current_player.name+" bought tile "+string+"\n"
                                mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                                mturn_history.change_f(mturn_history.string+turn_string)
                                success=True
                                break
                    if success:
                        break
                if not success:
                    continue
            #LEVELUP
            elif action=='l':
                success=False
                validValues=list(mpyramid.Pyramids.game_pyramids.get_pyramids_f(owner=current_player.name,level=-4))
                shuffle(validValues)
                for pyramid in validValues:
                    if pyramid==None or pyramid.level==4 or (pyramid.creator!=current_player.name and current_player.AI.choose_f(0,100)>70) or (pyramid.level<=mpyramid.Pyramids.game_pyramids.max_level_f(color=pyramid.color,name=current_player.name) and current_player.AI.choose_f(0,100)>90):
                        continue
                    nlevel=current_player.AI.choose_f(pyramid.level+1,4)
                    action_result=current_player.action_f("levelup", nlevel=nlevel, position=pyramid.position)
                    if action_result!=None:
                        continue
                    else:
                        turn_string="\t"+current_player.name+" upgraded the pyramid in position "+str(pyramid.position)+" to level "+str(nlevel)+"\n"
                        mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                        mturn_history.change_f(mturn_history.string+turn_string)
                        success=True
                        break
                if not success:
                    continue
            #PRAY
            elif action=='p':
                action_result=current_player.action_f("pray")
                if action_result!=None:
                    continue
                else:
                    turn_string="\t"+current_player.name+" prayed\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #MOVE
            elif action=='m':
                if strong==None or weak==None:
                    if mplayer.Player.AI_friendly:
                        taboo_part=None
                    else:
                        taboo_part=list(v.position for v in mpyramid.Pyramids.game_pyramids.get_pyramids_f(owner_in="AI_"))
                    troop_position,troop_dest,taboo=mboard.Board.game_board.get_interesting_positions_AI_f(current_player,taboo_part=taboo_part,AI_friendly=mplayer.Player.AI_friendly)
                    if troop_dest==None or troop_position==None:
                        troop_dest=None
                        board_positions=list(range(1,mboard.Board.game_board.map_max_position+1))
                        shuffle(board_positions)
                        for z in board_positions:
                            if (z in current_player.city and not mboard.Board.game_board.owned_by_someone_else_f(z,current_player)) or z in taboo:
                                continue
                            troop_dest=z
                            break
                        troops=list(mboard.Board.game_board.get_troops_f(player=current_player))
                        if len(troops)==0 or troop_dest==None:
                            continue
                        shuffle(troops)
                        for z in troops:
                            if mboard.Board.game_board.can_reach_f(troop_dest,z):
                                troop_position=z.position
                                break
                else:
                    troop_position=strong
                    troop_dest=weak
                if troop_position==None:
                    continue
                if 'm' in validActions:
                    action_result=current_player.action_f("move", position=troop_position, dest=troop_dest)
                elif 'g' in validActions:
                    action_result=current_player.action_f("gold", position=troop_position, dest=troop_dest, gold_sub='m')
                if action_result!=None:
                    mboard.Board.game_board.list_troops_f()
                    continue
                else:
                    turn_string="\t"+current_player.name+" moved from "+str(troop_position)+" to "+str(troop_dest)+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            #RECRUIT
            elif action=='r':
                recruit_array=[]            #[[quantity,position]]
                quantity_selected=0
                #reinforce existing troops if possible, then if enought units left for a good troop form as many as possible
                troops=mboard.Board.game_board.get_troops_f(player=current_player, possible_positions=current_player.city)
                taboo=[]
                troop_quantity_min=1
                if current_player.has_tile_f('Recruiting_scribe'):
                    troop_quantity_min=0
                for v in troops:
                    taboo.append(v.position)
                    troop_quantity_max=mplayer.Troop.max_quantity_f(current_player, troop=v, quantity_selected=quantity_selected)
                    if troop_quantity_max!=None and troop_quantity_max>=troop_quantity_min:
                        quantity_selected+=troop_quantity_max
                        recruit_array.append([troop_quantity_max, v.position])
                positions_all=list(v.position for v in mpyramid.Pyramids.game_pyramids.get_pyramids_f(creator=current_player.name))
                for v in positions_all:
                    if v in taboo:
                        continue
                    troop_existing=mboard.Board.game_board.get_troop_f(v)
                    troop_quantity_max=mplayer.Troop.max_quantity_f(current_player, troop=troop_existing, quantity_selected=quantity_selected)
                    if troop_quantity_max!=None and troop_quantity_max>=troop_quantity_min and (troop_quantity_max>=4 or (troop_existing!=None and troop_existing.player!=current_player and troop_existing.quantity+troop_existing.figure_strength+1<troop_quantity_max)):
                        quantity_selected+=troop_quantity_max
                        recruit_array.append([troop_quantity_max, v])

                if len(recruit_array)==0:
                    continue
                if 'r' in validActions:
                    action_result=current_player.action_f("recruit", recruit_array=recruit_array)
                elif 'g' in validActions:
                    action_result=current_player.action_f("gold", recruit_array=recruit_array, gold_sub='m')
                if action_result!=None:
                    continue
                else:
                    recruit_info=""
                    for v in recruit_array:
                        recruit_info=recruit_info+str(v[0])+" in "+str(v[1])+", "
                    recruit_info=recruit_info[0:-2]
                    turn_string="\t"+current_player.name+" recruited "+recruit_info+"\n"
                    mverbose.verbose_f(mverbose.Verbose(),"normal", turn_string)
                    mturn_history.change_f(mturn_history.string+turn_string)
            elif action=='c':
                available_cards=[]
                for z in current_player.DIcards:
                    if ('Raining_fire'==z.__class__.__name__ and (current_player.prayer.value>=1 or current_player.has_tile_f('Priest_of_Ra'))) or ('Prayer'==z.__class__.__name__ and current_player.prayer.value<10) or ('Enlistmen'==z.__class__.__name__ and current_player.troop_left>=2) or 'Mana_theft'==z.__class__.__name__:
                        available_cards.append(z.__class__.__name__)
                if len(available_cards)==0:
                    continue
                shuffle(available_cards)
                current_player.discard_DIcard_f(available_cards[0])
                continue
            else:
                mverbose.verbose_f(mverbose.Verbose(),"warning", "There is no action",action," \t",current_player.name," in turn",turn)
                continue
        else:       #just in case there is a bug somewhere else and lock states
            current_player.actions=current_player.actions_max

        current_player_index+=1
        attempts=[]
        init_AI=True
        troop_dest=None
        troop_position=None
        strong=None
        weak=None



    if mplayer.Player.players_have_actions_left_f()==False:
        game_players_temp=[]
        check_status_string=mboard.Board.game_board.get_troops_string_f()+mpyramid.Pyramids.game_pyramids.list_pyramids_string_f()+mplayer.get_players_string_f()+"\n\nTURN "+str(turn)+" ENDED: POINT STATUS\n"
        for z in mplayer.Player.game_players:
            troops=mboard.Board.game_board.get_troops_f(player=z, possible_positions=mboard.Board.game_board.temple)
            pyramids_level_4=len(mpyramid.Pyramids.game_pyramids.get_pyramids_f(owner=z.name, level=4))
            z_points=z.permanent_points +z.battle_points +len(troops) +pyramids_level_4
            game_players_temp.append([z, z_points])
            check_status_string=check_status_string+"\t"+z.name+" has "+str(z_points)+" points: \tpermanents="+str(z.permanent_points)+" \tbattle="+str(z.battle_points)+" \ttemples="+str(len(troops))+" \tpyramids="+str(pyramids_level_4)+"\n"
        for z in game_players_temp:
            for x in game_players_temp:
                if z[1]>x[1]:
                    index_z=game_players_temp.index(z)
                    index_x=game_players_temp.index(x)
                    if index_z<index_x:
                        temp=x
                        game_players_temp[index_x]=z
                        game_players_temp[index_z]=temp

        check_status_string=check_status_string+"New order: "+", ".join(list(v[0].name for v in game_players_temp))
        mverbose.verbose_f(mverbose.Verbose(),"normal", mturn_history.string)
        mverbose.verbose_f(mverbose.Verbose(),"normal", check_status_string)

        finished=False
        game_players_sorted=[]
        for z in game_players_temp:
            if z[1]>=10 and finished==False:
                finished=True
            game_players_sorted.append(z[0])
        if mplayer.Player.P_counter>0:
            curses.wrapper(mcurses.draw_menu_f, info=check_status_string, title="Turn end information")
        if finished:
            mverbose.verbose_f(mverbose.Verbose(),"normal", "\n\nThe game has finished in "+str(turn)+" turns\n\n")
            mverbose.verbose_f(mverbose.Verbose(),"info", "AI_friendly was",mplayer.Player.AI_friendly)
            exit(0)

        for z in game_players_sorted:
            z.night_phase_f()

        turn+=1
        mturn_history.change_f("\nTurn "+str(turn)+" log:\n")


mverbose.verbose_f(mverbose.Verbose(),"error", "Too many turns")
