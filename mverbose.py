from colorama import init
init()
from colorama import Fore, Back, Style

def verbose_f(verbose, verbose_type, *args):
    start=""
    reset=Style.RESET_ALL
    if verbose_type=="error":
        if verbose.everbose!=True:
            return 1
        start=Fore.RED
    elif verbose_type=="warning":
        if verbose.wverbose!=True:
            return 2
        start=Fore.YELLOW
    elif verbose_type=="info":
        if verbose.iverbose!=True:
            return 3
        start=Fore.BLUE
    elif verbose_type=="normal":
        if verbose.verbose!=True:
            return 4
    print(start, end='')
    for i in args:
        print(i, end=' ')
    print(reset)


class Verbose:
    def __init__(self,everbose=True,wverbose=True,iverbose=True,verbose=True):
        self.everbose=everbose
        self.wverbose=wverbose
        self.iverbose=iverbose
        self.verbose=verbose

