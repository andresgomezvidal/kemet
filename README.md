Game in Python I did just because I wanted to play without my friend (who owns the actual gameboard game) and just to practice a bit python (which I havent used in a long time).
The game interface is done with curses (keyaboard and text). I reckon I could have added graphic functionality but I don't have time anymore.

My main objective was to code an AI good enough for me to play against. My first approach was ifs with probabilities. I may try some kind of neural network in the future.

I don't have any kind of ownership rights over the Kemet gameboard. This is just for personal use.
I got permission from Matagot to publish this, since this software is pretty much useless and was made clearly for personal use. I mean I would not use it myself if I were to make a commercial game, I would prefer to start from scratch.


I made this in Python 3.7 on Linux to be run on a terminal emulator, so is possible you may have problems in the case you want to test it. In any case I attached it with this mail and I made a video for my friends some time ago: https://www.youtube.com/watch?v=gEs7I9ehhzM

I am sure there are still bugs and I made a lot of simplifications, so this is not really following the rules like it should (eg: there is no way to move and leaving troops behind). This is only the basegame, without any content from the expansions.
