import mplayer
import mverbose

class Pyramids:
    game_pyramids=None
    def __init__(self):
        self.pyramids=[]
        if Pyramids.game_pyramids==None:
            Pyramids.game_pyramids=self
    def get_pyramids_f(self,creator=None,owner=None,level=None,color=None,owner_in=None):
        if creator==None and owner==None and level==None and color==None and owner_in==None:
            return self.pyramids
        matches=[]
        for i in self.pyramids:
            if color!=None:
                if color!=i.color:
                    continue
            if creator!=None:
                if creator!=i.creator:
                    continue
            if owner!=None:
                if owner!=i.owner:
                    continue
            if level!=None:
                if level<0:
                    if -level<=i.level:
                        continue
                elif level!=i.level:
                    continue
            if owner_in!=None:
                if owner_in not in i.owner:
                    continue
            matches.append(i)
        return matches

    def list_pyramids_f(self,creator=None,owner=None,level=None,color=None):
        pyramids=self.get_pyramids_f(creator,owner,level,color)
        if creator!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing pyramids for creator",creator)
        if owner!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing pyramids for owner",owner)
        if level!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing pyramids for level",level)
        if color!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing pyramids for color",color)
        if creator==None and owner==None and level==None and color==None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing all pyramids")
        for i in pyramids:
            mverbose.verbose_f(mverbose.Verbose(),"info",
                "\tlevel=", i.level,
                "\tcolor=", i.color,
                "\tposition=", i.position,
                "\tcreator=", i.creator,
                "\towner=", i.owner
            )

    def list_pyramids_string_f(self,creator=None,owner=None,level=None,color=None):
        if creator==None and owner==None and level==None and color==None:
            string="Listing all pyramids:\n"
        else:
            string="Listing pyramids for"
        pyramids=self.get_pyramids_f(creator,owner,level,color)
        if creator!=None:
            string=string+" creator "+creator+","
        if owner!=None:
            string=string+" owner "+owner+","
        if level!=None:
            string=string+" level "+str(level)+","
        if color!=None:
            string=string+" owner "+color+","
        if creator!=None or owner!=None or level!=None or color!=None:
            string=string[0:-1]+"\n"
        for i in pyramids:
            string=string+ "\tcreator="+ i.creator+ " \towner="+ i.owner+ " \tposition="+ str(i.position) + " \tlevel="+ str(i.level)+ " \tcolor="+ i.color+"\n"
        return string


    def append_pyramid_f(self,pyramid):
        for i in self.pyramids:
            if i.position==pyramid.position:
                mverbose.verbose_f(mverbose.Verbose(),"error", "There is already a pyramid in",i.position)
                return 1
        self.pyramids.append(pyramid)
    def created_pyramid_f(self,position,creator):
        item=None
        for i in self.pyramids:
            if i.position==position:
               item=i
               break
        if item==None:
            mverbose.verbose_f(mverbose.Verbose(),"error", "There is no pyramid in",position)
            return False
        else:
            if item.creator==creator:
                return True
        return False
    def get_pyramid_f(self,position):
        for i in self.pyramids:
            if i.position==position:
                return i
        # mverbose.verbose_f(mverbose.Verbose(),"error", "There is no pyramid in",position)
        return None
    def levelup_f(self,nlevel,position,player, is_free=False):
        pyramid=self.get_pyramid_f(position)
        if pyramid==1:
            return 1
        if player.name==pyramid.owner:
            return pyramid.levelup_f(nlevel,player, is_free=is_free)
        else:
            mverbose.verbose_f(mverbose.Verbose(),"error", "The player",player.name,"is not the owner (",self.owner,") of the pyramid")
            return 2
    def max_level_f(self,color,name):
        temp=0
        for i in self.pyramids:
            if i.owner==name and i.color==color and temp<i.level:
                temp=i.level
        return temp
    def conquer_f(self,position,conqueror=None):
        for i in self.pyramids:
            if i.position==position:
                if conqueror==None:
                    i.owner=i.creator
                else:
                    i.owner=conqueror
                break

class Pyramid:
    def __init__(self,level,color,position,creator):
        if color!="red" and color!="blue" and color!="white":
            mverbose.verbose_f(mverbose.Verbose(),"error", "Cannot setup that color:",color)
            return 1
        self.level=level
        self.color=color
        self.position=position
        self.creator=creator
        self.owner=creator

    def verbose_f(self):
        mverbose.verbose_f(mverbose.Verbose(),"info",
            "info on pyramid:",
            "\tlevel=",
            self.level,
            "\tcolor=",
            self.color,
            "\tposition=",
            self.position,
            "\tcreator=",
            self.creator,
            "\towner=",
            self.owner)

    def levelup_f(self,nlevel, player, is_free=False):
        if self.level>=nlevel:
            mverbose.verbose_f(mverbose.Verbose(),"error", "The pyramid is already at level:",nlevel)
            return 1
        if self.level<nlevel and nlevel<5:
            cost=0
            for i in range(self.level+1,nlevel+1):
                cost+=i
            if player.has_tile_f('Slaves'):
                cost-=1*(nlevel-self.level)
            if player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0
            if is_free:
                cost=0
            if player.prayer.value>=cost:
                level_before=self.level
                player.prayer.change_f(-cost)
                self.level=nlevel
            else:
                mverbose.verbose_f(player.verbose,"warning", "Not enough prayer (",player.prayer.value,") to level from",self.level,"to",nlevel,"; it should be at least",cost)
                return 2
        else:
            mverbose.verbose_f(player.verbose,"error", "Cannot level",color,"from",self.level,"to",nlevel)
            return 3
