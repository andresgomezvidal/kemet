# https://gist.github.com/claymcleod/b670285f334acd56ad1c

import mplayer
import mboard
import mpyramid
import sys,os,curses

def draw_menu_f(stdscr,title="",info="",validValues=[],player=None,options_2=""):
    #type variables
    number=1
    string=""

    if info=="":
        info=mboard.Board.game_board.get_troops_string_f()+mpyramid.Pyramids.game_pyramids.list_pyramids_string_f()+mplayer.get_players_string_f()
    info=format_text_height_f(stdscr,info)

    validValues_2=[]
    if len(validValues)>0:
        if character_f(validValues):
            for i in validValues:
                if number.__class__.__name__==i.__class__.__name__:
                    validValues_2.append(str(i))
                elif string.__class__.__name__==i.__class__.__name__:
                    validValues_2.append(i.lower())
            options="Key options (ignorecase): ["+', '.join(str(v) for v in validValues)+"] "+options_2
        else:
            for i in validValues:
                if number.__class__.__name__==i.__class__.__name__:
                    validValues_2.append(str(i))
                elif string.__class__.__name__==i.__class__.__name__:
                    validValues_2.append(i.lower())
            options="Valid strings (ignorecase): ["+', '.join(str(v) for v in validValues)+"] "+options_2
        options=format_text_width_f(stdscr,options)
    else:
        options="Press any key to continue"

    info_index=0
    toogle_info2=True
    paint_f(stdscr,title,info[info_index],options)
    if character_f(validValues):
        k=stdscr.getch()          # get input character
        while(chr(k).lower() not in validValues_2 and chr(k).lower()!='q'):
            info_index,toogle_info2=toogle_f(k,player,stdscr,title,info,info_index,toogle_info2,options)
            if len(validValues)==0 and k!=curses.KEY_UP and k!=curses.KEY_DOWN and k!=curses.KEY_RIGHT and k!=curses.KEY_LEFT:
                return chr(k).lower()
            k = stdscr.getch()
        return chr(k).lower()
    else:
        k=stdscr.getstr()         # get input string
        while(k.decode(encoding="utf-8").lower() not in validValues_2 and k.decode(encoding="utf-8").lower()!='q'):
            info_index,toogle_info2=toogle_f(k,player,stdscr,title,info,info_index,toogle_info2,options) #working only for down
            k = stdscr.getstr()
        return k.decode(encoding="utf-8").lower()

def toogle_f(k,player,stdscr,title,info,info_index,toogle_info2,options):
    if k==curses.KEY_UP or k==curses.KEY_DOWN or str(k)=="b''":
        if player!=None:
            if toogle_info2:
                info2=mboard.Board.game_board.get_troops_string_f(player=player)+mpyramid.Pyramids.game_pyramids.list_pyramids_string_f(owner=player.name)+mplayer.get_players_string_f([player])
                paint_f(stdscr,title,info2,options)
            else:
                info_index+=1
                if info_index>=len(info):
                    info_index=0
                paint_f(stdscr,title,info[info_index],options)
            toogle_info2=not toogle_info2
    elif k==curses.KEY_RIGHT:
        info_index+=1
        if info_index>=len(info):
            info_index=0
        paint_f(stdscr,title,info[info_index],options)
    elif k==curses.KEY_LEFT:
        info_index-=1
        if info_index<0:
            info_index=len(info)-1
        paint_f(stdscr,title,info[info_index],options)
    return info_index,toogle_info2

def format_text_width_f(stdscr,options):
    height, width = stdscr.getmaxyx()
    wlimit=width-20
    if len(options)>wlimit:
        return options[0:wlimit-3]+"..."
    else:
        return options

def format_text_height_f(stdscr,info):
    height, width = stdscr.getmaxyx()
    hlimit=height-6
    count=0
    arr=[]
    tarr=""
    try:
        lines=info.split('\n')
        for i in lines:
            count+=1+len(i)//width #count lines and wrapps
            if count<hlimit:
                tarr=tarr+i+'\n'
            else:
                count=0
                arr.append(tarr+i+'\n'+"-"*20)
                tarr=""
        if count>0:
            arr.append(tarr)
    except AttributeError:
        tarr=info

    if len(arr)==0:
        return [tarr]
    else:
        return arr


def paint_f(stdscr,title,info,options):
    stdscr.clear()

    # Start colors in curses
    curses.start_color()
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

    height, width = stdscr.getmaxyx()

    # Centering calculations
    start_x_title = int((width // 2) - (len(title) // 2) - len(title) % 2)
    start_y = int((height // 4)*4 - 2)

    # Rendering some text
    stdscr.addstr(0, 0, info, curses.color_pair(1))

    # Render status bar
    stdscr.attron(curses.color_pair(3))
    stdscr.addstr(height-1, 0, options)
    stdscr.addstr(height-1, len(options), " " * (width - len(options) - 1))
    stdscr.attroff(curses.color_pair(3))

    # Turning on attributes for title
    stdscr.attron(curses.color_pair(2))
    stdscr.attron(curses.A_BOLD)

    # Rendering title
    stdscr.addstr(start_y, start_x_title, title)

    # Turning off attributes for title
    stdscr.attroff(curses.color_pair(2))
    stdscr.attroff(curses.A_BOLD)

    stdscr.move(height-1, width-1)

    # Refresh the screen
    stdscr.refresh()

def character_f(lista):
    for i in lista:
        if len(str(i))>1:
            return False
    return True
