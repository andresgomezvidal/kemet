import mtile
import mverbose
import mDIcards
import mBcards
import mboard
import mpyramid
import mAI
import mturn_history
import mcurses, curses
from random import shuffle


def init_game_f(AI_ask=False,only_AI=False):
    if mboard.Board.game_board==None:
        mboard.Board(3)
    if mDIcards.GameDICards.game_DIcards==None:
        mDIcards.GameDICards()
    if mtile.GameTiles.game_tiles==None:
        mtile.GameTiles()
    if mpyramid.Pyramids.game_pyramids==None:
        mpyramid.Pyramids()


    auto=False
    if not only_AI:
        ok=curses.wrapper(mcurses.draw_menu_f, info="Automate creation of players?", validValues=['y','n'], title="Automate creation of players?", options_2="for [Yes, No]")
        if ok=='y':
            auto=True
    players=[]
    player_counter=1
    AI_counter=1
    for i in range(1,mboard.Board.game_board.players+1):
        city=[mboard.Board.game_board.pyramid[3*(i-1)],mboard.Board.game_board.pyramid[3*(i-1)+1],mboard.Board.game_board.pyramid[3*(i-1)+2]]
        #continue instead of indents
        if AI_ask or only_AI:
            if AI_ask:
                AI=curses.wrapper(mcurses.draw_menu_f, info="Creation of "+"P/AI_"+str(i), validValues=['y','n'], title="Is this player controlled by AI?", options_2="for [Yes, No]")
            if only_AI:
                AI='y'
            if AI=='y':
                # neural=curses.wrapper(mcurses.draw_menu_f, info="Creation of "+"AI_"+str(AI_counter), validValues=['y','n'], title="Neural network?", options_2="for [Yes, No]")
                neural='n'
                if neural=='y':
                    tAI=mAI.AI(neural=True)
                    #TODO
                else:
                    tAI=mAI.AI()
                    rlevel=tAI.choose_f(0,2)
                    if rlevel==0:
                        blevel=tAI.choose_f(1,2)
                    else:
                        blevel=tAI.choose_f(0,min(2,3-rlevel))
                    wlevel=3-rlevel-blevel
                    rtroops=min(rlevel,1)*5
                    btroops=min(blevel,1)*5
                    wtroops=10-rtroops-btroops
                players.append(Player("AI_"+str(AI_counter), city, [rlevel,blevel,wlevel], [rtroops,btroops,wtroops],AI=tAI))
                AI_counter+=1
                continue

        if auto:
            players.append(Player("P_"+str(player_counter), city, [1,1,1], [5,5,0]))
            player_counter+=1
            continue


        vplayer="error"
        levels=[]
        troops=[]
        perror=""
        while vplayer!="":
            pinfo=perror+"Creation of "+"P_"+str(player_counter)
            rlevel=int(curses.wrapper(mcurses.draw_menu_f, info=pinfo, validValues=[0,1,2], title="Select level of red pyramid"))
            pinfo=pinfo+"\n\tLevel of red pyramid="+str(rlevel)
            blevel=int(curses.wrapper(mcurses.draw_menu_f, info=pinfo, validValues=list(range(0,min(4-rlevel,3))), title="Select level of blue pyramid"))
            pinfo=pinfo+"\n\tLevel of blue pyramid="+str(blevel)
            wlevel=3-rlevel-blevel
            pinfo=pinfo+"\n\tLevel of white pyramid="+str(wlevel)
            rtroops=int(curses.wrapper(mcurses.draw_menu_f, info=pinfo, validValues=list(range(0,6)), title="Select quantity of troops for red pyramid"))
            pinfo=pinfo+"\n\tTroops in red pyramid="+str(rtroops)
            btroops=int(curses.wrapper(mcurses.draw_menu_f, info=pinfo, validValues=list(range(0,6)), title="Select quantity of troops for blue pyramid"))
            pinfo=pinfo+"\n\tTroops in blue pyramid="+str(btroops)
            wtroops=10-rtroops-btroops
            pinfo=pinfo+"\n\tTroops in white pyramid="+str(wtroops)
            ok=curses.wrapper(mcurses.draw_menu_f, info=pinfo, validValues=['y','n'], title="Is this ok?", options_2="for [Yes, No]")
            if ok!='y':
                perror=""
                continue
            levels=[rlevel,blevel,wlevel]
            troops=[rtroops,btroops,wtroops]

            vplayer=Player.validate_f(levels, troops)
            perror=vplayer+"\n\n"
        players.append(Player("P_"+str(player_counter), city, levels, troops))
        player_counter+=1
    return players

def get_players_string_f(players=None):
    if players==None:
        string="Listing all players:\n"
        players=Player.game_players
    else:
        if len(players)>1:
            string="Listing some players:\n"
        else:
            string="Listing a player:\n"
    for i in players:
        string=string+"\t"+i.get_player_string_f()+"\n"
    return string


class Prayer:
    def __init__(self):
        self.value=7
    def change_f(self,n):
        self.value+=n
        if self.value>11:
            self.value=11
        elif self.value<0:
            self.value=0


class Player:
    game_players=[]
    AI_counter=0
    P_counter=0
    AI_secret_DIcards=False
    AI_friendly=False
    default_actions=["move","move","pray","pray","levelup","recruit","red","blue","white"]

    def validate_f(levels, troops,  everbose=True,wverbose=True,iverbose=True,verbose=True, AI=None):
        tverbose=mverbose.Verbose(everbose,wverbose,iverbose,verbose)

        error=""
        if levels[0]+levels[1]+levels[2]!=3:
            error=error+"The sum of the levels of your pyramids should be 3, instead of "+str(levels[0]+levels[1]+levels[2])+"\n"
        if levels[0]>2 or levels[1]>2 or levels[2]>2:
            error=error+"The starting level of a pyramid should be 0, 1 or 2, but never 3\n"
        if troops[0]+troops[1]+troops[2]!=10:
            error=error+"The sum of the troops should be 10, instead of "+str(troops[0]+troops[1]+troops[2])+"\n"
        if troops[0]>5 or troops[1]>5 or troops[2]>5:
            error=error+"A player cannot place more than 5 units in a troop at the start\n"
        if (levels[0]<1 and troops[0]>0) or (levels[1]<1 and troops[1]>0) or (levels[2]<1 and troops[2]>0):
            error=error+"Cannot place troops in a position without a pyramid (or level 0)\n"
        if levels[0]<0 or levels[1]<0 or levels[2]<0:
            error=error+"Cannot start with negative pyramid levels\n"
        if error!="":
            mverbose.verbose_f(tverbose,"error", error)
        return error

    def __init__(self, name, city, levels, troops, everbose=True,wverbose=True,iverbose=True,verbose=True, AI=None):
        self.verbose=mverbose.Verbose(everbose,wverbose,iverbose,verbose)
        mverbose.verbose_f(self.verbose,"info", "Creating player",name)
        self.name=name
        self.tiles=mtile.Tiles()
        self.prayer=Prayer()
        self.actions_available=list(Player.default_actions)
        self.actions_max=5
        self.permanent_points=0
        self.battle_points=0
        self.actions=0
        self.troop_max=5
        self.troop_left=12
        self.figures_free=[]
        self.city=city
        self.AI=AI
        self.Bcards=mBcards.initial_cards_f()
        self.DIcards=[]
        self.draw_DI_f(1)
        mpyramid.Pyramids.game_pyramids.append_pyramid_f(mpyramid.Pyramid(levels[0],"red",city[0],self.name))
        mpyramid.Pyramids.game_pyramids.append_pyramid_f(mpyramid.Pyramid(levels[1],"blue",city[1],self.name))
        mpyramid.Pyramids.game_pyramids.append_pyramid_f(mpyramid.Pyramid(levels[2],"white",city[2],self.name))
        Troop(troops[0],city[0],self,init=True)
        Troop(troops[1],city[1],self,init=True)
        Troop(troops[2],city[2],self,init=True)
        if AI==None:
            Player.P_counter+=1
        else:
            Player.AI_counter+=1
        Player.game_players.append(self)


    def chance_pray_f(self,attempts):
        if len(attempts)>10:
            return 0
        if self.prayer.value==11:
            return 100
        chance=20 +self.prayer.value*8
        if self.has_tile_f('Priest'):
            chance-=15
        if self.has_tile_f('Priestess'):
            chance+=5
        if self.has_tile_f('Priest_of_Ra'):
            chance+=10
        return min(chance,90)

    def chance_levelup_f(self,attempts):
        if 'levelup' in attempts:
            return 100
        chance=80 -self.prayer.value*5 -(min(self.permanent_points+self.battle_points,9))*2
        pyramid_levels=mpyramid.Pyramids.game_pyramids.max_level_f(color="red",name=self.name) + mpyramid.Pyramids.game_pyramids.max_level_f(color="blue",name=self.name) +mpyramid.Pyramids.game_pyramids.max_level_f(color="white",name=self.name)
        chance+=pyramid_levels*3
        if mpyramid.Pyramids.game_pyramids.max_level_f(color="red",name=self.name)==4:
            chance+=5
        if mpyramid.Pyramids.game_pyramids.max_level_f(color="blue",name=self.name)==4:
            chance+=5
        if mpyramid.Pyramids.game_pyramids.max_level_f(color="white",name=self.name)==4:
            chance+=5
        if self.has_tile_f('Slaves'):
            chance-=40
        if self.has_tile_f('Priest_of_Ra'):
            chance-=8
        if self.has_tile_f('Hand_of_God'):
            chance+=30
        if self.prayer.value==0 and not self.has_tile_f('Priest_of_Ra') and not self.has_tile_f('Slaves'):
            return 100
        return min(chance,90)

    def chance_recruit_f(self,attempts):
        if self.troop_left<1 or 'recruit' in attempts:
            return 100
        chance=120 -self.prayer.value*2 -min(20*len(self.figures_free),40) -10*self.troop_left
        if self.has_tile_f('Recruiting_scribe'):
            chance-=25
        if self.has_tile_f('Priest_of_Ra'):
            chance-=8
        if self.has_tile_f('Divine_will'):
            chance-=15
        if self.prayer.value==0 and not self.has_tile_f('Priest_of_Ra') and not self.has_tile_f('Recruiting_scribe'):
            return 100
        return min(chance,90)

    def chance_buy_f(self,turn,attempts):
        if 'buy' in attempts:
            return 100
        chance=65 +max(turn*5,30) -self.prayer.value*5
        if "red" in self.actions_available:
            chance-=5
        if "blue" in self.actions_available:
            chance-=5
        if "white" in self.actions_available:
            chance-=5
        if self.has_tile_f('Priestess'):
            chance-=15
        if self.has_tile_f('Priest_of_Ra'):
            chance-=15
        if self.prayer.value==0 and not self.has_tile_f('Priest_of_Ra') and not self.has_tile_f('Priestess'):
            return 100
        return min(chance,90)

    def chance_move_f(self,strong,weak,attempts):
        if strong!=None and weak!=None:
            return 0
        chance=90 -self.actions*20 +self.troop_left*3 +attempts.count('move')*2
        if self.has_tile_f('Crusade'):
            if self.prayer.value<3:
                chance-=5*10//(self.prayer.value+1)
            elif self.prayer.value<7:
                chance-=2*10//self.prayer.value
            else:
                chance-=1*10//self.prayer.value
        if self.has_tile_f('Divine_will'):
            chance-=30
        return min(chance,90)

    def get_player_string_f(self):
        if Player.AI_secret_DIcards and self.AI!=None:
            return self.name+ ":\tPpoinst="+ str(self.permanent_points+self.battle_points)+ " \tPrayer="+ str(self.prayer.value)+ " \tActions_available="+ ",".join(self.actions_available)+ " \tActions_left="+ str(self.actions_max-self.actions) \
            + " \tTiles="+ ",".join(self.tiles.get_names_f())+ " \tDIcards="+ str(len(self.DIcards))+ " \tFigures_free="+ ",".join(self.figures_free)
        else:
            return self.name+ ":\tPpoinst="+ str(self.permanent_points+self.battle_points)+ " \tPrayer="+ str(self.prayer.value)+ " \tActions_available="+ ",".join(self.actions_available)+ "\tActions_left="+ str(self.actions_max-self.actions) \
            + " \tTiles="+ ",".join(self.tiles.get_names_f())+ " \tDIcards="+ ",".join(self.list_DIcards_f())+ " \tFigures_free="+ ",".join(self.figures_free)

    def status_f(self):
        mverbose.verbose_f(mverbose.Verbose(),"info",
            "info on player:", self.name,
            "\tPpoinst=", self.permanent_points+self.battle_points,
            "\tprayer=", self.prayer.value,
            "\tactions_available=", self.actions_available,
            "\ttiles=", self.tiles.get_names_f(),
            "\tactions=", self.actions,
            "\tactions_max=", self.actions_max,
            "\tDIcards=", self.list_DIcards_f(),
            "\tfigures_free=", self.figures_free
        )

    def has_tile_f(self,string):
        for i in self.tiles.list:
            if string==i.__class__.__name__:
                return True
        return False

    def has_DIcard_f(self,string):
        for i in self.DIcards:
            if string.lower()==i.__class__.__name__.lower():
                return True
        return False

    def list_DIcards_f(self,kind=None):
        total=[]
        for i in self.DIcards:
            if kind!=None:
                if i.kind!=kind:
                    continue
            total.append(i.__class__.__name__)
        return total

    def discard_DIcard_f(self,string):
        return mDIcards.GameDICards.game_DIcards.use_f(string,self)

    def discard_DIcards_f(self,quantity):
        if quantity>len(self.DIcards):
            mDIcards.GameDICards.game_DIcards.restore_some_cards_f(self.DIcards,name=self.name)
            self.DIcards=[]
        else:
            shuffle(self.DIcards)
            mDIcards.GameDICards.game_DIcards.restore_some_cards_f(self.DIcards[-quantity:len(self.DIcards)],name=self.name)
            self.DIcards=self.DIcards[0:len(self.DIcards)-quantity]

    def draw_DI_choosen_f(self):
        DIcards=[]
        for i in range(5):
            card=mDIcards.GameDICards.game_DIcards.get_random_card_f()
            if card==None:
                break
            DIcards.append(card)
        if len(DIcards)==0:
            return
        if self.AI==None:
            cards_string="Listing randomly drawed cards to choose from\n"
            for j in DIcards:
                cards_string=cards_string+"\t"+j.__class__.__name__+" \t\tcost="+str(j.cost)+"\n"
            card_index=int(curses.wrapper(mcurses.draw_menu_f, validValues=list(range(0,5)), info=cards_string, title="Index of the card to draw", player=self))
        else:
            #sort by initial quantity (more -> more position)
            for i in DIcards:
                for j in DIcards:
                    if i.initial_quantity>j.initial_quantity:
                        index_i=DIcards.index(i)
                        index_j=DIcards.index(j)
                        if index_i<index_j:
                            temp=j
                            DIcards[index_j]=i
                            DIcards[index_i]=temp
            # card_index=DIcards[0]
            # card_index=self.AI.choose_f(0,len(DIcards)-1)
        self.DIcards.append(DIcards[0])
        DIcards.pop(0)
        mDIcards.GameDICards.game_DIcards.restore_some_cards_f(DIcards)

    def draw_DI_f(self,quantity):
        for i in range(0,quantity):
            temp=mDIcards.GameDICards.game_DIcards.get_random_card_f()
            if temp==None:
                return
            self.DIcards.append(temp)

    def night_phase_f(self):
        self.prayer.change_f(2)
        draw_cards=1
        if self.has_tile_f('High_priest'):
            self.prayer.change_f(2)
        if self.has_tile_f('Priest_of_Amon'):
            self.prayer.change_f(5)
        if self.has_tile_f('Divine_boon'):
            draw_cards+=1
        if self.has_tile_f('The_Mummy'):
            draw_cards+=1
        if self.has_tile_f('Vision'):
            #Vision does not stack, only once per night phase at most
            self.draw_DI_choosen_f()
            draw_cards-=1
            self.draw_DI_f(draw_cards)
        else:
            self.draw_DI_f(draw_cards)

        #SACRIFICES FOR POINTS/PRAYER
        troops=mboard.Board.game_board.get_troops_f(player=self)
        for i in troops:
            if i.position==1 and i.quantity>=2:                   #sacrifice 2 quantity for 1 permanent_point
                if (self.AI!=None) or (self.AI==None and curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f(troops=troops)+get_players_string_f([self]), validValues=['y','n'], title="Night phase: Do you want to sacrifice 2 units for 1 permanent point in "+str(i.position)+"?", options_2="for [Yes, No]")=='y'):
                    self.permanent_points+=1
                    i.change_f(-2)
                    if i.quantity<1:
                        mboard.Board.game_board.remove_troop_f(i)
            if i.position==3 and i.quantity>=1:                   #sacrifice 1 quantity for 5 prayer
                if (self.AI!=None and self.prayer.value<3) or \
                (self.AI==None and curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f(player=self)+get_players_string_f([self]), validValues=['y','n'], title="Night phase: Do you want to sacrifice 1 unit for 5 prayer in "+str(i.position)+"?", options_2="for [Yes, No]")=='y'):
                    self.prayer.change_f(5)
                    i.change_f(-1)
                    if i.quantity<1:
                        mboard.Board.game_board.remove_troop_f(i)

        #TEMPLE POINTS/PRAYER
        troops=mboard.Board.game_board.get_troops_f(player=self, possible_positions=mboard.Board.game_board.temple)
        for i in troops:
            if i.position==15 or i.position==28:
                self.prayer.change_f(2)
            elif i.position==10 or i.position==21:
                self.prayer.change_f(3)
        self.permanent_points+=len(troops)//2

        #NIGHT BENEFITS
        if self.has_tile_f('Reinforcements'):
            if self.troop_left>0:
                troops=mboard.Board.game_board.get_troops_f(player=self)
                shuffle(troops)
                free_units=min(self.troop_left,4)
                for i in troops:
                    this_free_units=min(free_units,self.troop_max-i.quantity)
                    if self.troop_max>i.quantity and this_free_units>0:
                        if self.AI==None:
                            units=int(curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f(player=self)+get_players_string_f([self]), validValues=list(range(0,this_free_units+1)), title="Reinforcements remaining="+str(free_units)+". Choose how many units for position "+str(i.position), options_2="you can choose 0 in this position"))
                        else:
                            units=self.AI.choose_f(0,this_free_units)
                        if units>0:
                            free_units-=units
                            Troop.recruit_f(units,i.position,self, is_free=True)
                if free_units>0:
                    for i in self.city:
                        troop=mboard.Board.game_board.get_troop_f(i)
                        if troop!=None:                                     #if troop, then it was checked before
                            continue
                        if free_units==0:
                            break
                        if self.AI==None:
                            units=int(curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f(player=self)+get_players_string_f([self]), validValues=list(range(0,free_units+1)), title="Reinforcements remaining="+str(free_units)+". Choose how many units for position "+str(i), options_2="you can choose 0 in this position"))
                        else:
                            units=self.AI.choose_f(0,free_units)
                        if units>0:
                            free_units-=units
                            Troop.recruit_f(units,i,self, is_free=True)

        if self.has_tile_f('Hand_of_God'):
            pyramids=mpyramid.Pyramids.game_pyramids.get_pyramids_f(owner=self.name,level=-4)
            if len(pyramids)>0:
                pyramid=None
                if len(pyramids)>1:
                    if self.AI==None:
                        position=int(curses.wrapper(mcurses.draw_menu_f, info=mpyramid.Pyramids.game_pyramids.list_pyramids_string_f(owner=self.name)+get_players_string_f([self]), validValues=list(v.position for v in pyramids), title="Position of the pyramid to raise 1 level for free"))
                        pyramid=mpyramid.Pyramids.game_pyramids.get_pyramid_f(position)
                    else:
                        shuffle(pyramids)
                        for i in reversed(range(4)):
                            for j in pyramids:
                                if j.level==i and self.AI.choose_f(0,100)>20:
                                    pyramid=mpyramid.Pyramids.game_pyramids.get_pyramid_f(j.position)
                                    break
                            if pyramid!=None:
                                break
                if pyramid==None:
                    pyramid=pyramids[0]
                mpyramid.Pyramids.game_pyramids.levelup_f(pyramid.level+1,pyramid.position,self, is_free=True)

        #RESET player variables
        self.actions=0
        self.actions_available=list(Player.default_actions)
        if self.has_tile_f('Divine_will'):
            self.actions_available.append('gold')

    def pray_f(self):
        if self.has_tile_f('Priest'):
            self.prayer.change_f(3)
        else:
            self.prayer.change_f(2)

    def action_success_f(self,index):
        self.actions_available.pop(index)
        self.actions+=1

    #recruit_array=[[quantity,position],[quantity,position]]
    def action_f(self,action_type,nlevel=0,position=-1,recruit_array=[[]],dest=-1,string="",color="",gold_sub=None):
        if self.actions<self.actions_max or 'gold' in self.actions_available:
            if action_type=="buy":
                if color=="":
                    color=mtile.GameTiles.game_tiles.get_color_f(string)
                if color=="red" or color=="blue" or color=="white":
                    action_type=color
                else:
                    mverbose.verbose_f(self.verbose,"error", "Could not find the color of tile",string)
                    return 1
            try:
                index=self.actions_available.index(action_type)
            except:
                mverbose.verbose_f(self.verbose, "warning", "There are no remaining actions for",action_type)
                return 2

            mverbose.verbose_f(self.verbose,"info", "Player",self.name,"uses action '",action_type,"'")
            if action_type=="pray":
                self.pray_f()
                self.action_success_f(index)
            elif action_type=="levelup":
                if color=="red":
                    position=self.city[0]
                elif color=="blue":
                    position=self.city[1]
                elif color=="white":
                    position=self.city[2]
                if mpyramid.Pyramids.game_pyramids.levelup_f(nlevel,position,self)==None:
                    self.action_success_f(index)
                else:
                    return 1
            elif action_type=="move" or gold_sub=='m':
                troop=mboard.Board.game_board.get_troop_f(position)
                if troop!=None:
                    temp=mboard.Board.game_board.move_f(dest,troop)
                    if temp.__class__.__name__!=nlevel.__class__.__name__:
                        self.action_success_f(index)
                    else:
                        mverbose.verbose_f(self.verbose,"error", "Error",temp,"moving troop from",position,"to",dest)
                        return 1
                else:
                    mverbose.verbose_f(self.verbose,"error", "There is no troop in",position)
                    return 1
                mboard.Board.game_board.list_troops_f()
            elif action_type=="recruit" or gold_sub=='r':
                if Troop.recruit_array_f(recruit_array,self)!=None:
                    mverbose.verbose_f(self.verbose,"error", "Cannot recruit",recruit_array,"")
                    self.status_f()
                    return 1
                self.action_success_f(index)
                mboard.Board.game_board.list_troops_f()
            elif action_type=="red" or action_type=="blue" or action_type=="white":
                if self.buy_tile_f(string)==None:
                    self.action_success_f(index)
                else:
                    return 1
            self.status_f()
        else:
            mverbose.verbose_f(self.verbose, "warning", "There are no actions left for player",self.name)
            return 3


    def buy_tile_f(self,string):
        if self.has_tile_f(string):
            mverbose.verbose_f(self.verbose,"error", "The player",self.name,"already owns the tile",string)
            return 1
        prayer_before=self.prayer.value
        item=mtile.GameTiles.game_tiles.buy_f(string,self,mpyramid.Pyramids.game_pyramids)
        temp=1
        if item.__class__.__name__!=temp.__class__.__name__:
            # mverbose.verbose_f(self.verbose,"normal", "Player",self.name,"bought tile",item.__class__.__name__)
            self.tiles.list.append(item)
        else:
            if item==1:
                mverbose.verbose_f(self.verbose,"error", "Cannot find tile",string)
            elif item==2:
                mverbose.verbose_f(self.verbose,"error", "Not enough level with the red pyramid (",mpyramid.Pyramids.game_pyramids.max_level_f("red",self.name),") of player",self.name," for",string)
            elif item==3:
                mverbose.verbose_f(self.verbose,"error", "Not enough level with the blue pyramid (",mpyramid.Pyramids.game_pyramids.max_level_f("blue",self.name),") of player",self.name," for",string)
            elif item==4:
                mverbose.verbose_f(self.verbose,"error", "Not enough level with the white pyramid (",mpyramid.Pyramids.game_pyramids.max_level_f("white",self.name),") of player",self.name," for",string)
            elif item==5:
                mverbose.verbose_f(self.verbose,"warning", "Player",self.name,"cannot buy tile: not enough quantity or prayer (",self.prayer.value,")")
            return item

    def list_tiles_f(self):
        mverbose.verbose_f(self.verbose,"info", "Listing tiles of player",self.name)
        self.tiles.list_names_f()

    def player_have_actions_left_f(self):
        if self.actions_max-self.actions>0 or 'gold' in self.actions_available:
            return True
        return False

    def players_have_actions_left_f():
        for i in Player.game_players:
            if i.actions_max-i.actions>0 or 'gold' in i.actions_available:
                return True
        return False

    def get_validValues_actions_f(self,kind):
        validValues=[]
        if kind=="actions":
            if "red" in self.actions_available or "blue" in self.actions_available or "white" in self.actions_available:
                validValues.append('b')
            if "levelup" in self.actions_available:
                validValues.append('l')
            if "pray" in self.actions_available:
                validValues.append('p')
            if "move" in self.actions_available:
                validValues.append('m')
            if "recruit" in self.actions_available and self.troop_left>0:
                validValues.append('r')
            if "gold" in self.actions_available:
                validValues.append('g')
            if self.has_DIcard_f('Raining_fire') or self.has_DIcard_f('Prayer') or (self.has_DIcard_f('Enlistmen') and self.troop_left>0) or self.has_DIcard_f('Mana_theft'):
                validValues.append('c')

        elif kind=="buy_color":
            if "red" in self.actions_available:
                validValues.append('r')
            if "blue" in self.actions_available:
                validValues.append('b')
            if "white" in self.actions_available:
                validValues.append('w')
        return validValues

    def new_figure_f(self,string):
        troops=mboard.Board.game_board.get_troops_f(player=self, figure_existing=False)
        self.figures_free.append(string)
        for i in troops:
            if i.ask_add_figure_f(figure_string=string):
                return









class Troop:
    def validate_troop_f(quantity,position,player,is_free=False):
        if quantity==0:
            mverbose.verbose_f(player.verbose,"warning", "Cannot place 0 units in a troop")
            return 1
        if quantity>player.troop_max:
            mverbose.verbose_f(player.verbose,"error", "You cannot have more than",player.troop_max,"troops")
            return 1
        if quantity>player.troop_left:
            mverbose.verbose_f(player.verbose,"error", "You cannot have",player.troop_left+quantity,"troops active, only 12 as max")
            return 1
        if mpyramid.Pyramids.game_pyramids.created_pyramid_f(position,player.name)!=True and not is_free:
            mverbose.verbose_f(player.verbose,"error", "You cannot place troops in the position",position)
            return 1

    def recruit_array_f(recruit_array,player):
        #recruit_array=[[quantity,position],[quantity,position]]
        if len(recruit_array)==1 and recruit_array[0][0]==0:
            if player.has_tile_f('Recruiting_scribe'):
                recruit_array[0][0]=2
            else:
                return 1
        quantity_total=0
        for row in recruit_array:
            quantity=row[0]
            quantity_total+=quantity
            position=row[1]
            if Troop.validate_troop_f(quantity,position,player)!=None:
                return 1
        cost=quantity_total
        if player.has_tile_f('Recruiting_scribe'):
            cost-=2
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if cost<0:
            cost=0
        if cost>player.prayer.value:
            mverbose.verbose_f(player.verbose,"error", "You cannot pay",cost,"(you only have",player.prayer.value,") for",quantity,"troops")
            return 1

        player.prayer.change_f(-cost)
        for row in recruit_array:
            quantity=row[0]
            position=row[1]
            troop_in_position=mboard.Board.game_board.get_troop_f(position)
            if troop_in_position!=None and troop_in_position.player==player:
                troop_in_position.change_f(quantity)
                troop_in_position.ask_add_figure_f()
            else:
                Troop(quantity,position,player,cost=0)

    def recruit_f(quantity,position,player, is_free=False):
        if Troop.validate_troop_f(quantity,position,player,is_free=is_free)!=None:
            return 1
        cost=quantity
        if player.has_tile_f('Recruiting_scribe'):
            cost-=2
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if cost<0:
            cost=0
        if is_free:
            cost=0
        if cost>player.prayer.value:
            mverbose.verbose_f(player.verbose,"error", "You cannot pay",cost,"(you only have",player.prayer.value,") for",quantity,"troops")
            return 1

        troop_in_position=mboard.Board.game_board.get_troop_f(position)
        if troop_in_position!=None and troop_in_position.player==player:
            troop_in_position.change_f(quantity)
            player.prayer.change_f(-cost)
            troop_in_position.ask_add_figure_f()
            return troop_in_position
        else:
            return Troop(quantity,position,player,cost=cost)

    def __init__(self,quantity,position,player,cost=None,init=False):
        if quantity<1:
            return None
        if cost==None:
            cost=quantity
            if player.has_tile_f('Recruiting_scribe'):
                cost-=2
            if player.has_tile_f('Priest_of_Ra'):
                cost-=1
            if cost<0:
                cost=0

        if init==False:
            if cost>player.prayer.value:
                mverbose.verbose_f(player.verbose,"error", "You cannot pay",cost,"(you only have",player.prayer.value,") for",quantity,"troops")
                return None
            else:
                player.prayer.change_f(-cost)

        player.troop_left-=quantity
        self.quantity=quantity
        self.position=position
        self.player=player
        self.distance=1
        self.figure_strength=0
        self.figure_defense=0
        self.figure_damage=0
        self.figure_nullify=False
        self.figure=""
        self.ask_add_figure_f()

        troop_in_position=mboard.Board.game_board.get_troop_f(position)
        if troop_in_position!=None and troop_in_position.player!=player:
            mboard.Board.game_board.troops.append(self)
            mboard.Board.game_board.attack_f(self,position)
        else:
            mboard.Board.game_board.move_troop_f(self,position)

    def ask_add_figure_f(self,figure_string=None):
        if len(self.player.figures_free)==0 or self.figure!="":
            return False
        if self.player.AI==None:
            info_figures="Listing available figures for "+self.player.name+":\n\t"+'\n\t'.join(self.player.figures_free)
            if figure_string==None:
                figure_temp_string="a figure"
            else:
                figure_temp_string="the figure "+figure_string
            ok=curses.wrapper(mcurses.draw_menu_f, validValues=['y','n'], title="Do you want to place "+figure_temp_string+" in the troop on position "+str(self.position)+"?", options_2="for [Yes, No]",info=mboard.Board.game_board.get_troops_string_f()+mpyramid.Pyramids.game_pyramids.list_pyramids_string_f()+get_players_string_f()+"\n\n"+info_figures, player=self)
            if ok!='y':
                return False
            if figure_string==None:
                if len(self.player.figures_free)==1:
                    figure_string=self.player.figures_free[0]
                else:
                    figure_string=curses.wrapper(mcurses.draw_menu_f, validValues=self.player.figures_free, title="Name of the figure to place", player=self.player, info=info_figures)
                    for i in self.player.figures_free:
                        if i.lower()==figure_string:
                            figure_string=i
                            break
        else:
            if figure_string==None:
                shu=list(self.player.figures_free)
                shuffle(shu)
                figure_string=shu[0]
        return self.add_figure_f(figure_string)

    def add_figure_f(self,string):
        # if self.figure!="":
            # mverbose.verbose_f(self.player.verbose,"error", "Player",self.name,"cannot add figure",string,"to",position,"because there is already",self.figure)
            # return 1
        try:
            self.player.figures_free.pop(self.player.figures_free.index(string))
            self.figure=string
            if string=="Royal_Scarab":
                self.distance=3
                return True
            self.distance=2
            if string=="Giant_Scorpion":
                self.figure_strength=2
                self.figure_damage=2
            if string=="Ancestral_Elephant":
                self.figure_strength=1
                self.figure_defense=1
            if string=="Deep_Desert_Snake":
                self.figure_nullify=True
            if string=="Phoenix" or string=="Sphinx" or string=="The_Mummy":
                self.figure_strength=2
        except:
            mverbose.verbose_f(self.player.verbose,"error", "Player",self.player.name,"cannot add figure",string,"to",self.position,"because is not available")
            return False

    def change_f(self,quantity):
        self.quantity+=quantity
        self.player.troop_left+=-quantity
        if self.player.troop_left>12:
            self.player.troop_left=12
        elif self.player.troop_left<0:
            self.player.troop_left=0
        if self.quantity>self.player.troop_max:
            self.quantity=self.player.troop_max
        elif self.quantity<0:
            self.quantity=0
            #the check to remove the troop is done in the caller function to avoid None found


    def max_quantity_f(player, troop=None, quantity_selected=0):
        if troop!=None and troop.player==player:
            cost=troop.quantity+quantity_selected
        else:
            cost=quantity_selected
        if player.has_tile_f('Recruiting_scribe'):
            cost-=2
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        can_pay=player.prayer.value-cost
        if can_pay<0:
            return None
        if player.AI==None:
            if troop!=None and troop.player==player:
                return max(min(player.troop_left-quantity_selected, player.troop_max-troop.quantity) ,0)
            else:
                return max(min(player.troop_left-quantity_selected, player.troop_max) ,0)
        else:
            if troop!=None and troop.player==player:
                return max(min(min(player.troop_left-quantity_selected, player.troop_max-troop.quantity), can_pay) ,0)
            else:
                return max(min(min(player.troop_left-quantity_selected, player.troop_max), can_pay) ,0)







if len(Player.game_players)==0:
    init_game_f(AI_ask=True)
    # init_game_f(AI_ask=False,only_AI=True)
    if Player.P_counter>0:
        Player.AI_friendly=True
        Player.AI_secret_DIcards=True
