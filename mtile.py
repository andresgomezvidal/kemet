import mplayer
import mBcards
import mverbose
import mboard
import mcurses, curses

#RED
class Charge:
    def __init__(self):
        self.quantity=2
        self.color="red"
        self.kind="attack"
        self.cost=1
    def buy_f(self,player):
        pass

class Stargate:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="movement"
        self.cost=1
    def buy_f(self,player):
        pass

class God_Speed:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="movement"
        self.cost=1
    def buy_f(self,player):
        pass

class Carnage:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="battle"
        self.cost=2
    def buy_f(self,player):
        pass

class Offensive_Strategy:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="card"
        self.cost=2
    def buy_f(self,player):
        mBcards.exchange_cards_f(player,player.Bcards,"red")

class Open_Gates:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="movement"
        self.cost=2
    def buy_f(self,player):
        pass

class Teleport:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="movement"
        self.cost=2
    def buy_f(self,player):
        pass

class Royal_Scarab:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="figure"
        self.cost=3
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)

class Blades_of_Neith:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="battle"
        self.cost=3
    def buy_f(self,player):
        pass

class Divine_wound:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="battle"
        self.cost=3
    def buy_f(self,player):
        pass

class Giant_Scorpion:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="figure"
        self.cost=4
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)

class Initiative:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="before battle"
        self.cost=4
    def buy_f(self,player):
        pass

class Phoenix:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="figure"
        self.cost=4
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)



#BLUE
class Recruiting_scribe:
    def __init__(self):
        self.quantity=2
        self.color="blue"
        self.kind="recruit"
        self.cost=1
    def buy_f(self,player):
        pass

class Defense:
    def __init__(self):
        self.quantity=2
        self.color="blue"
        self.kind="defense"
        self.cost=1
    def buy_f(self,player):
        pass

class Legion:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="troop"
        self.cost=2
    def buy_f(self,player):
        player.troop_max=7

class Ancestral_Elephant:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="figure"
        self.cost=2
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)

class Defensive_strategy:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="card"
        self.cost=2
    def buy_f(self,player):
        mBcards.exchange_cards_f(player,player.Bcards,"blue")

class Deep_Desert_Snake:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="figure"
        self.cost=2
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)

class Shield_of_Neith:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="battle"
        self.cost=3
    def buy_f(self,player):
        pass

class Defensive_victory:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="after battle win"
        self.cost=3
    def buy_f(self,player):
        pass

class Prescience:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="after opponent battle card"
        self.cost=3
    def buy_f(self,player):
        pass

class Reinforcements:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="night"
        self.cost=4
    def buy_f(self,player):
        pass

class Sphinx:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="figure"
        self.cost=4
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)
        player.permanent_points+=1

class Divine_will:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="golden action"
        self.cost=4
    def buy_f(self,player):
        player.actions_available.append('gold')


#WHITE
class Priest:
    def __init__(self):
        self.quantity=2
        self.color="white"
        self.kind="pray"
        self.cost=1
    def buy_f(self,player):
        pass

class Priestess:
    def __init__(self):
        self.quantity=2
        self.color="white"
        self.kind="tile"
        self.cost=1
    def buy_f(self,player):
        pass

class Slaves:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="levelup"
        self.cost=2
    def buy_f(self,player):
        pass

class High_priest:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="night"
        self.cost=2
    def buy_f(self,player):
        pass

class Crusade:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="after battle"
        self.cost=2
    def buy_f(self,player):
        pass

class Divine_boon:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="night"
        self.cost=2
    def buy_f(self,player):
        pass

class Hand_of_God:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="night"
        self.cost=3
    def buy_f(self,player):
        pass

class Vision:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="night"
        self.cost=3
    def buy_f(self,player):
        pass

class Holy_war:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="after battle win"
        self.cost=3
    def buy_f(self,player):
        pass

class Priest_of_Ra:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="prayer"
        self.cost=4
    def buy_f(self,player):
        pass

class Priest_of_Amon:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="night"
        self.cost=4
    def buy_f(self,player):
        pass

class The_Mummy:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="figure"
        self.cost=4
    def buy_f(self,player):
        player.new_figure_f(__class__.__name__)


#COMMON
class Victory_Point_red:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="point"
        self.cost=3
    def buy_f(self,player):
        player.permanent_points+=1

class Victory_Point_blue:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="point"
        self.cost=3
    def buy_f(self,player):
        player.permanent_points+=1

class Victory_Point_white:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="point"
        self.cost=3
    def buy_f(self,player):
        player.permanent_points+=1


class Act_of_God_red:
    def __init__(self):
        self.quantity=1
        self.color="red"
        self.kind="action"
        self.cost=4
    def buy_f(self,player):
        player.actions_max+=1

class Act_of_God_blue:
    def __init__(self):
        self.quantity=1
        self.color="blue"
        self.kind="action"
        self.cost=4
    def buy_f(self,player):
        player.actions_max+=1

class Act_of_God_white:
    def __init__(self):
        self.quantity=1
        self.color="white"
        self.kind="action"
        self.cost=4
    def buy_f(self,player):
        player.actions_max+=1





class Tiles:
    #cost -2 means cost less or equal than 2
    def get_tiles_f(self,color=None,kind=None,cost=None,quantity=None):
        if color==None and kind==None and quantity==None and cost==None:
            return self.list
        temp=[]
        for i in self.list:
            if color!=None:
                if color!=i.color:
                    continue
            if kind!=None:
                if kind!=i.kind:
                    continue
            if quantity!=None:
                if (quantity<0 and i.quantity>0) or quantity==i.quantity:
                    pass
                else:
                    continue
            if cost!=None:
                if cost<0:
                    if -cost>=i.cost:
                        pass
                    else:
                        continue
                else:
                    if cost!=i.cost:
                        continue
            temp.append(i)
        return temp

    def get_names_f(self,color=None,kind=None,cost=None,quantity=None,tiles=None):
        if tiles==None:
            tiles=self.get_tiles_f(color,kind,quantity,cost)
        temp=[]
        for i in tiles:
            temp.append(i.__class__.__name__)
        return temp

    def list_names_available_string_f(self,color=None,kind=None,cost=None,tiles=None):
        if tiles==None:
            tiles=self.get_tiles_f(color,kind,cost)
        string=""
        if color!=None:
            string=string+"Listing tiles for color="+color+"\n"
        if kind!=None:
            string=string+"Listing tiles for type="+kind+"\n"
        if cost!=None:
            if cost<0:
                string=string+"Listing tiles for cost<="+str(-cost)+"\n"
            else:
                string=string+"Listing tiles for cost="+str(cost)+"\n"
        if color==None and kind==None and cost==None:
            string=string+"Listing all tiles\n"
        for i in tiles:
            if i.quantity>0 and i.__class__.__name__ not in string:
                string=string+"\t"+i.__class__.__name__+"\n"
        return string

    def list_names_f(self,color=None,kind=None,cost=None,quantity=None,tiles=None):
        if tiles==None:
            tiles=self.get_tiles_f(color,kind,quantity,cost)
        if color!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing tiles for color=",color)
        if kind!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing tiles for type=",kind)
        if quantity!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing tiles for quantity=",quantity)
        if cost!=None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing tiles for cost=",cost)
        if color==None and kind==None and quantity==None and cost==None:
            mverbose.verbose_f(mverbose.Verbose(),"info", "Listing all tiles")
        for i in tiles:
            mverbose.verbose_f(mverbose.Verbose(),"info", "\t",i.__class__.__name__)

    def __init__(self):
        self.list=[]


class GameTiles(Tiles):
    game_tiles=None
    def get_color_f(self,string):
        for i in self.list:
            if string.lower()==i.__class__.__name__.lower():
                return i.color

    def get_available_f(self):
        avaiable=[]
        for i in self.list:
            if i.quantity>0:
                avaiable.append(i)
        return avaiable

    def buy_f(self,string,player,game_pyramids):
        item=None
        for i in self.list:
            if string.lower()==i.__class__.__name__.lower():
                item=i
                break
        if item==None:
            return 1

        cost=item.cost
        if player.has_tile_f('Priestess'):
            cost-=1
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1

        if item.color=="red" and game_pyramids.max_level_f("red",player.name)<cost:
            return 2
        if item.color=="blue" and game_pyramids.max_level_f("blue",player.name)<cost:
            return 3
        if item.color=="white" and game_pyramids.max_level_f("white",player.name)<cost:
            return 4

        if item.quantity>0 and player.prayer.value>=cost:
            item.quantity-=1
            player.prayer.change_f(-cost)
            item.buy_f(player)
            return item
        else:
            return 5

    def __init__(self):
        self.list=[]
        self.list.append(Charge())
        self.list.append(Stargate())
        self.list.append(God_Speed())
        self.list.append(Carnage())
        self.list.append(Offensive_Strategy())
        self.list.append(Open_Gates())
        self.list.append(Teleport())
        self.list.append(Royal_Scarab())
        self.list.append(Blades_of_Neith())
        self.list.append(Divine_wound())
        self.list.append(Giant_Scorpion())
        self.list.append(Initiative())
        self.list.append(Phoenix())
        self.list.append(Recruiting_scribe())
        self.list.append(Defense())
        self.list.append(Legion())
        self.list.append(Ancestral_Elephant())
        self.list.append(Defensive_strategy())
        self.list.append(Deep_Desert_Snake())
        self.list.append(Shield_of_Neith())
        self.list.append(Defensive_victory())
        self.list.append(Prescience())
        self.list.append(Reinforcements())
        self.list.append(Sphinx())
        self.list.append(Divine_will())
        self.list.append(Priest())
        self.list.append(Priestess())
        self.list.append(Slaves())
        self.list.append(High_priest())
        self.list.append(Crusade())
        self.list.append(Divine_boon())
        self.list.append(Hand_of_God())
        self.list.append(Vision())
        self.list.append(Holy_war())
        self.list.append(Priest_of_Ra())
        self.list.append(Priest_of_Amon())
        self.list.append(The_Mummy())
        self.list.append(Victory_Point_red())
        self.list.append(Victory_Point_blue())
        self.list.append(Victory_Point_white())
        self.list.append(Act_of_God_red())
        self.list.append(Act_of_God_blue())
        self.list.append(Act_of_God_white())
        if GameTiles.game_tiles==None:
            GameTiles.game_tiles=self
