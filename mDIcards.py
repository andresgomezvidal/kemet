import mplayer
import mboard
import mpyramid
import curses, mcurses
import mverbose
import mturn_history
from random import shuffle

#BATTLE
class War_rage:
    def __init__(self):
        self.quantity=3
        self.kind="battle"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass

class War_fury:
    def __init__(self):
        self.quantity=3
        self.kind="battle"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        cost=self.cost
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if not player.prayer.value>=cost:
            mturn_history.change_f(mturn_history.string+" (but could not pay)")
            return
        player.prayer.change_f(-cost)

class Bloody_battle:
    def __init__(self):
        self.quantity=3
        self.kind="battle"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass

class Bloodbath:
    def __init__(self):
        self.quantity=2
        self.kind="battle"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        cost=self.cost
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if not player.prayer.value>=cost:
            mturn_history.change_f(mturn_history.string+" (but could not pay)")
            return
        player.prayer.change_f(-cost)

class Bronze_wall:
    def __init__(self):
        self.quantity=3
        self.kind="battle"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass

class Iron_wall:
    def __init__(self):
        self.quantity=2
        self.kind="battle"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        cost=self.cost
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if not player.prayer.value>=cost:
            mturn_history.change_f(mturn_history.string+" (but could not pay)")
            return
        player.prayer.change_f(-cost)



#DAY
class Raining_fire:
    def __init__(self):
        self.quantity=4
        self.kind="day"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        cost=self.cost
        if player.has_tile_f('Priest_of_Ra'):
            cost-=1
        if not player.prayer.value>=cost:
            mturn_history.change_f(mturn_history.string+" (but could not pay)")
            return
        player.prayer.change_f(-cost)
        enemies=mboard.Board.game_board.get_troops_f(not_owner=player.name, AI_friendly=mplayer.Player.AI_friendly, player=player)
        for z in enemies:
            for x in enemies:
                if z.quantity>x.quantity:
                    index_z=enemies.index(z)
                    index_x=enemies.index(x)
                    if index_z<index_x:
                        temp=x
                        enemies[index_x]=z
                        enemies[index_z]=temp
        if len(enemies)==0:
            return
        target=enemies[0]
        if player.AI==None:
            for i in enemies:
                if curses.wrapper(mcurses.draw_menu_f, info=mboard.Board.game_board.get_troops_string_f(troops=enemies)+mplayer.get_players_string_f([player]), validValues=['y','n'], title="Raining_fire DIcard: Do you want to kill 1 unit in position "+str(i.position)+"?", options_2="for [Yes, No]")=='y':
                    target=i
                    break
        target.change_f(-1)
        if target.quantity<1:
            mboard.Board.game_board.remove_troop_f(target)

class Prayer:
    def __init__(self):
        self.quantity=2
        self.kind="day"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        player.prayer.change_f(2)

class Enlistmen:
    def __init__(self):
        self.quantity=4
        self.kind="day"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        if player.troop_left<1:
            return
        troops=mboard.Board.game_board.get_troops_f(player=player)
        info=mboard.Board.game_board.get_troops_string_f(troops=troops)+"\n"+mplayer.get_players_string_f([player])
        for i in troops:
            if i.quantity<player.troop_max:
                units_available=min(player.troop_left, min(2, player.troop_max-i.quantity))
                if (player.AI!=None) or (player.AI==None and curses.wrapper(mcurses.draw_menu_f, info=info, validValues=['y','n'], title="Enlistmen DIcard: Do you want to add "+str(units_available)+" units in position "+str(i.position)+"?", options_2="for [Yes, No]")=='y'):
                    mplayer.Troop.recruit_f(units_available, i.position, player, is_free=True)
                    return
        info=mpyramid.Pyramids.game_pyramids.list_pyramids_string_f(owner=player.name)+"\n"+mplayer.get_players_string_f([player])
        for i in player.city:
            troop=mboard.Board.game_board.get_troop_f(i)
            if troop!=None:                                     #if troop, then it was checked before
                continue
            if (player.AI!=None) or (player.AI==None and curses.wrapper(mcurses.draw_menu_f, info=info, validValues=['y','n'], title="Enlistmen DIcard: Do you want to add 2 units in position "+str(i)+" (which is pyramid without units)?", options_2="for [Yes, No]")=='y'):
                mplayer.Troop.recruit_f(2, i, player, is_free=True)
                return

class Mana_theft:
    def __init__(self):
        self.quantity=2
        self.kind="day"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        player.prayer.change_f(1)
        for i in mplayer.Player.game_players:
            if i!=player:
                i.prayer.change_f(-1)


#DAY: movement
class Teleportation:
    def __init__(self):
        self.quantity=3
        self.kind="day"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass
        #done in mboard

class Open_gates:
    def __init__(self):
        self.quantity=1
        self.kind="day"
        self.cost=1
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass
        #done in mboard



#CANCEL
class Veto:
    def __init__(self):
        self.quantity=2
        self.kind="cancel"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        #cannot be used during a battle
        pass

class Escape:
    def __init__(self):
        self.quantity=1
        self.kind="cancel"
        self.cost=0
        self.initial_quantity=self.quantity
    def use_f(self,player):
        pass





class GameDICards:
    game_DIcards=None
    def restore_some_cards_f(self,cards,name=None):
        for j in cards:
            for i in self.list:
                if j.__class__.__name__.lower()==i.__class__.__name__.lower():
                    if name!=None:
                        mverbose.verbose_f(mverbose.Verbose(),"normal", "Discarded DIcard",i.__class__.__name__,"from player",name)
                        mturn_history.change_f(mturn_history.string+" \t"+name+" discarded "+i.__class__.__name__+" \t\t")
                    i.quantity+=1
                    break
        self.shuffle_f()

    def list_cards_string_f(self,cards):
        string="Listing available cards:\n"
        for i in self.list:
            for j in cards:
                if i==j:
                    string=string+"\t"+j.__class__.__name__+": \tcost="+str(i.cost)+"\n"
        return string

    def get_random_card_f(self):
        item=None
        for i in self.list:
            if i.quantity>0:
                item=i
                break
        if item==None:
            return None
        item.quantity-=1
        self.shuffle_f()
        return item

    def find_or_veto_card_f(self,string,player):
        for i in self.list:
            if string.lower()==i.__class__.__name__.lower():
                mverbose.verbose_f(mverbose.Verbose(),"normal", "Discarded DIcard",i.__class__.__name__,"from player",player.name)
                mturn_history.change_f(mturn_history.string+" \t"+player.name+" discarded "+i.__class__.__name__+" \t\t")
                player.DIcards.pop(player.DIcards.index(i))
                if i.kind=="day":
                    for p in mplayer.Player.game_players:
                        if p==player or not p.has_DIcard_f('Veto'):
                            continue
                        if p.AI==None:
                            if curses.wrapper(mcurses.draw_menu_f, validValues=['y','n'], title=p.name+": Use Veto to cancel DIcard "+i.__class__.__name__+" from "+player.name+"?", options_2="for [Yes, No]")=='y':
                                p.discard_DIcard_f('Veto')
                                return None
                        elif not mplayer.Player.AI_friendly:
                            if p.AI.choose_f(1,10)>6:
                                p.discard_DIcard_f('Veto')
                                return None
                return i

    def use_f(self,string,player):
        item=self.find_or_veto_card_f(string,player)
        if item==None:
            return 1
        item.quantity+=1
        item.use_f(player)

    def shuffle_f(self):
        shuffle(self.list)

    def __init__(self):
        self.list_used=[]
        self.list=[]
        self.list.append(War_rage())
        self.list.append(War_fury())
        self.list.append(Bloody_battle())
        self.list.append(Bloodbath())
        self.list.append(Bronze_wall())
        self.list.append(Iron_wall())
        self.list.append(Raining_fire())
        self.list.append(Prayer())
        self.list.append(Enlistmen())
        self.list.append(Mana_theft())
        self.list.append(Open_gates())
        self.list.append(Teleportation())
        self.list.append(Veto())
        self.list.append(Escape())
        self.shuffle_f()
        if GameDICards.game_DIcards==None:
            GameDICards.game_DIcards=self

